
# Spending iOS

## Cocoapod
因為 Cocoapod 可能大幅的增加編譯時間，因此只在大小不大的 library 使用

Note: QMUI 使用 Carthage 編譯出來的 Framework 缺少 ARM64，暫時用 cocoapod 替代

## Cathage
可直接編譯出 Framework，XCode 不需重新編譯，速度快，但配置繁瑣，版本不能一直更新，適合大的的 libray
### 使用名單
* Realm
* Chart

## Fastlane
使用 Fastlane 來避免重複性的人工操作

### 自動打包
fastlane release

### 自動截圖
fastlane screenshots