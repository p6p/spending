//
//  SpendingUITests.swift
//  SpendingUITests
//
//  Created by master on 2018/1/24.
//  Copyright © 2018年 test. All rights reserved.
//

import XCTest

class SpendingUITests: XCTestCase {
    let app = XCUIApplication()

    override func setUp() {
        super.setUp()
        
        continueAfterFailure = false
        Snapshot.setupSnapshot(app)
        app.launch()
    }

    func testExample() {
        snapshot("01")
        app.buttons.matching(identifier: "流水").element.tap()
        snapshot("02")
        app.buttons.matching(identifier: "圖表").element.tap()
        snapshot("03")
        app.buttons.matching(identifier: "首頁").element.tap()
        app.buttons.matching(identifier: "+").element.tap()
        snapshot("04")

    }
    
}
