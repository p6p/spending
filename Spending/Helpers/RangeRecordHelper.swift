//
//  RangeRecordHelper.swift
//  Spending
//
//  Created by master on 2018/3/2.
//  Copyright © 2018年 test. All rights reserved.
//

import Foundation
import RealmSwift

class RangeRecordHelper: NSObject {
    static let sharedInstance = RangeRecordHelper()
    let realm = try! Realm()
    
    func getDateComponents(from date: Date) -> DateComponents {
        var dateComponents = calendar.dateComponents([.year,.month, .day, .hour,.minute,.second], from: date)
        dateComponents.hour = 0
        dateComponents.minute = 0
        dateComponents.second = 0
        return dateComponents
    }
    
    func getMonthDayCount(date: Date) -> Int {
        return calendar.range(of: .day, in: .month, for: date)!.count
    }
    
    func getDayRecord(date: Date) -> [Record] {
        
        let startDate = date // day
        let endDate = calendar.date(byAdding: .day, value: 1, to: startDate) // day + 1
        let results = realm.objects(Record.self).filter("date >= %@ AND date < %@", startDate as NSDate, endDate! as NSDate)
        
        return Array(results)
    }
    
    
    // 陣列的第一層按天組成
    func getDayRecrordsList(queryDate: Date) -> [[Record]] {
        var dayRecordsList : [[Record]] = []
        
        var queryDateComponents = getDateComponents(from: queryDate)
        
        let range = getMonthDayCount(date: queryDate)
        for i in  (1...range).reversed() {
            queryDateComponents.day = i
            
            let dayRecord = getDayRecord(date: calendar.date(from: queryDateComponents)!)
            
            if dayRecord.count > 0 {
                dayRecordsList.append(dayRecord)
            }
        }
        
        return dayRecordsList
    }
}
