//
//  UserManger.swift
//  Spending
//
//  Created by master on 2018/3/7.
//  Copyright © 2018年 test. All rights reserved.
//

import Foundation

enum UserType : Int {
    case free = 0, plus, pro
}

class UserManger {
    static let sharedInstance = UserManger()
    
    var userType: UserType = .free
    var expireDate: Date?

    func set(userType: UserType) {
        UserDefaults.standard.set(userType, forKey: kUserType)
    }
    
    func getUserType() -> UserType? {
        return UserDefaults.standard.object(forKey: kUserType) as! UserType?
    }
    
    func set(expireDate :Date) {
        UserDefaults.standard.set(expireDate, forKey: kExpireDate)
    }
    
    func getExpireDate() -> Date? {
        return UserDefaults.standard.object(forKey: kExpireDate) as! Date?
    }
}
