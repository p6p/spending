//
//  PasswordManger.swift
//  Spending
//
//  Created by master on 2018/3/6.
//  Copyright © 2018年 test. All rights reserved.
//

import Foundation
import TOPasscodeViewController

class PasswordManger: NSObject, TOPasscodeViewControllerDelegate {
    static let sharedInstance = PasswordManger()
    
    let settingModel = SettingModel()
    var completionHandler: ((Bool)->Void)?
    
    func disablePassword() {
        settingModel.set(enablePassword: false)
    }
    
    // 要做 present 操作的 ViewController
    func setNewPassword(in viewController: UIViewController, completion: ((Bool)->Void)) {
        let passwordController = TOPasscodeViewController(style: .translucentLight, passcodeType: .fourDigits)
        passwordController.delegate = self
        viewController.present(passwordController, animated: true, completion: nil)
    }
    
    // MARK: - ---------- password input----------
    // 終止輸入密碼
    internal func didTapCancel(in passcodeViewController: TOPasscodeViewController) {
        completionHandler?(false)
    }
    
    // 輸入了密碼
    internal func passcodeViewController(_ passcodeViewController: TOPasscodeViewController, isCorrectCode code: String) -> Bool {
        completionHandler?(true)
        settingModel.set(enablePassword: true)
        settingModel.set(launchPassword: code)
        return true
    }
}
