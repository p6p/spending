//
//  ConfigurationManger.swift
//  Spending
//
//  Created by master on 2018/1/27.
//  Copyright © 2018年 test. All rights reserved.
//

import UIKit
import Firebase

class ConfigurationManger: NSObject {
    static let sharedInstance = ConfigurationManger()
    let remoteConfig = RemoteConfig.remoteConfig()

    func setDefault() {
        #if DEBUG
            let remoteConfigSettings = RemoteConfigSettings(developerModeEnabled: true)
            remoteConfig.configSettings = remoteConfigSettings!
        #endif
        let defaultConfig : [String:NSObject]? = ["pro_setting" : true as NSObject]
        remoteConfig.setDefaults(defaultConfig)
    }
    
    func fetch(expirationDuration: Int = 43200) {
        remoteConfig.fetch(withExpirationDuration: TimeInterval(expirationDuration)) { (status, error) -> Void in
            if status == .success {
                self.remoteConfig.activateFetched()
            }
        }
    }
    
    let paddingHorizontal = 15
    let spacingHorizontal = 15
    let recordListCellHeight: CGFloat = 45
    let recordListCellSumLabelTextSize: CGFloat = 16
    let recordListCellCategoryNameLabelTextSize: CGFloat = 16
    let recordListTableViewSeparatorColor = UIColor.clear
    let recordListTableViewSectionHeight: CGFloat = 20
    

}
