//
//  IapMangaer.swift
//  
//
//  Created by master on 2018/3/6.
//

import Foundation
import StoreKit

class IapMangaer: NSObject, SKPaymentTransactionObserver, SKProductsRequestDelegate {
    static let sharedInstance = IapMangaer()
    let productId : Set<String> = ["com.utree.spendingPro"]
    let receiptVerify = ReceiptVerify()
    
    var products : [SKProduct] = []
    
    deinit {
        SKPaymentQueue.default().remove(self)
    }
    
    var hasObserver = false
    var requestProductSuccess: (()->Void)?
    func requestProduct(success: @escaping (()->Void)) {
        requestProductSuccess = success
        if SKPaymentQueue.canMakePayments() {
            if !hasObserver {
                SKPaymentQueue.default().add(self)
                hasObserver = true
            }
            let request = SKProductsRequest(productIdentifiers: productId)
            request.delegate = self
            request.start()
        }
    }
    
    // MARK: - ---------SKProductsRequestDelegate--------
    func productsRequest(_ request: SKProductsRequest, didReceive response: SKProductsResponse) {
        requestProductSuccess?()
        products = response.products
        if response.invalidProductIdentifiers.count != 0 {
            print(response.invalidProductIdentifiers.description)
        }
    }
    
    // 自動續訂確認
    func recheckSubscription(success: (()->Void)?, fail: (()->Void)?) {
        if UserManger.sharedInstance.getUserType() == .pro {
            if UserManger.sharedInstance.getExpireDate()! < Date() {
                receiptVerify.verifyTransation(fromeUserDefault: true, isSnadbox: false, success: success, fail: fail)
            }
        }
    }
    
    // 初次購買
    var buySuccess: (()->Void)?
    var buyFail: (()->Void)?
    func buy(success: @escaping (()->Void), fail: @escaping (()->Void)) {
        if products.count > 0 {
            buySuccess = success
            buyFail = fail
            let payment = SKPayment(product: products[0])
            let queue = SKPaymentQueue.default()
            queue.add(payment)
        }
    }
    
    // 回復購買
    var restoreSuccess: (()->Void)?
    var restoreFail: (()->Void)?
    func restore(success: @escaping (()->Void), fail: @escaping (()->Void)) {
        restoreSuccess = success
        restoreFail = fail
        SKPaymentQueue.default().restoreCompletedTransactions()
    }
    
    // MARK: - ---------SKPaymentTransactionObserver---------
    func paymentQueue(_ queue: SKPaymentQueue, updatedTransactions transactions: [SKPaymentTransaction]) {
        for i in transactions {
            switch i.transactionState {
            case .purchased:
                receiptVerify.verifyTransation(success: {
                    self.buySuccess?()
                }, fail: {
                    self.buyFail?()
                })
                queue.finishTransaction(i)
            case .failed:
                self.buyFail?()
                self.restoreFail?()
                queue.finishTransaction(i)
                break
            case .deferred:
                break
            case .purchasing:
                break
            case .restored:
                receiptVerify.verifyTransation(success: {
                    self.restoreSuccess?()
                }, fail: {
                    self.restoreFail?()
                })
                queue.finishTransaction(i)
            }
        }
    }
}
