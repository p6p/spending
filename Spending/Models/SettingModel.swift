//
//  SettingModel.swift
//  Spending
//
//  Created by master on 2018/3/6.
//  Copyright © 2018年 test. All rights reserved.
//

import Foundation

class SettingModel {
    let kEnablePassword = "useAuthentication"
    let kLaunchPassword = "password"
    
    static let sharedInstance = SettingModel()
    
    func enablePassword() -> Bool {
        return UserDefaults.standard.bool(forKey: kEnablePassword)
    }
    
    func set(enablePassword: Bool) {
        let defaults = UserDefaults.standard
        defaults.set(enablePassword, forKey: kEnablePassword)
        defaults.synchronize()
    }
    
    func launchPassword() -> String {
        return UserDefaults.standard.string(forKey: kLaunchPassword)!
    }
    
    func set(launchPassword: String) {
        let defaults = UserDefaults.standard
        defaults.set(launchPassword, forKey: kLaunchPassword)
        defaults.synchronize()
    }
    
    let kEnableReminder = "enable_reminder"
    func enableReminder() -> Bool {
        return  UserDefaults.standard.bool(forKey: kEnableReminder)
    }
    
    func set(enableReminder: Bool) {
        let defaults = UserDefaults.standard
        defaults.set(enableReminder, forKey: kEnableReminder)
        defaults.synchronize()
    }
    
    let kReminderTime = "reminder_time"
    func reminderTime() -> Date? {
        return UserDefaults.standard.object(forKey: kReminderTime) as! Date?
    }
    
    func set(reminderTime: Date) {
        let defaults = UserDefaults.standard
        defaults.set(reminderTime, forKey: kReminderTime)
        defaults.synchronize()
    }
}
