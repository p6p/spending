//
//  GroupData.swift
//  Spending
//
//  Created by master on 2018/1/16.
//  Copyright © 2018年 test. All rights reserved.
//  從 DB 中抓取一段時間的資料，處理成圖表要用的格式

import Foundation
import RealmSwift

let calendar = Calendar.current

// MARK: Operation Chart Data

/// 在一週中，每天統計一次 Balance, Income, Outcome
func getWeekOperationAndGroupByDay(queryDate : Date = Date()) -> [[String: Double]] {
    var result : [[String: Double]] = []
    var dateComponents =  calendar.dateComponents([.year, .month, .weekOfMonth, .weekday], from: queryDate)
    
    for i in  1...7 {
        dateComponents.weekday = i
        
        let qDate1 = calendar.date(from: dateComponents)! // day
        let qDate2 = calendar.date(byAdding: .day, value: 1, to: qDate1) // day + 1
        result.append(durationOperation(qDate1, qDate2))
    }
    return result
}

/// 在一個月中，每天統計一次 Balance, Income, Outcome
func getMonthOperationAndGroupByDay(queryDate : Date = Date()) -> [[String: Double]] {
    var result : [[String: Double]] = []
    var dateComponents =  calendar.dateComponents([.year,.month, .day, .hour,.minute,.second], from: queryDate)
    
    let range = calendar.range(of: .day, in: .month, for: queryDate) // 該月份範圍
    for i in  1...(range?.count)! {
        dateComponents.day = i
        
        let qDate1 = calendar.date(from: dateComponents)! // day
        let qDate2 = calendar.date(byAdding: .day, value: 1, to: qDate1)
        result.append(durationOperation(qDate1, qDate2))
    }
    return result
}

/// 在一年中，每月統計一次 Balance, Income, Outcome
func getYearOperationAndGroupByMonth(queryDate : Date = Date()) -> [[String: Double]] {
    var result : [[String: Double]] = []
    var dateComponents =  calendar.dateComponents([.year,.month, .day], from: queryDate)

    for i in  1...12 {
        dateComponents.month = i
        dateComponents.day = 1
        let qDate1 = calendar.date(from: dateComponents)! // day
        let qDate2 = calendar.date(byAdding: .month, value: 1, to: qDate1)
        result.append(durationOperation(qDate1, qDate2))
    }
    return result
}

/// 給定一段時間，回傳該段時間的 income, outcome, balance，不將類別分開
func durationOperation(_ qDate1: Date, _ qDate2: Date?) -> Dictionary<String, Double> {
    let realm = try! Realm()
    let results = realm.objects(Record.self).filter("date >= %@ AND date < %@", qDate1 as NSDate, qDate2! as NSDate)
    
    var balance = 0.0
    var income = 0.0
    var outcome = 0.0
    for i in results {
        if (i.category?.isIncome)! {
            balance += i.sum
            income += i.sum
        } else {
            balance -= i.sum
            outcome -= i.sum
        }
    }
    return ["balance" : balance, "income" : income, "outcome" : outcome]
}

// MARK: - Pie Chart Data

/// [
///    RecordCategory.Food : ["Income": 20, "Outcome": 5, "Balance": 5]
/// ]
/// getWeekOperationAndGroupByCategory
func getWeekOperationAndGroupByCategory(queryDate : Date = Date()) -> [RecordCategory:[String: Double]] {
    var dateComponents =  calendar.dateComponents([.year, .month, .weekOfMonth, .weekday], from: queryDate)
    dateComponents.weekday = 1
    
    // query duration
    let qDate1 = calendar.date(from: dateComponents)!
    let qDate2 = calendar.date(byAdding: .day, value: 7, to: qDate1)
    return groupByDateAndCategory(qDate1, qDate2)
}

/// [
///    RecordCategory.Food : ["Income": 20, "Outcome": 5, "Balance": 5]
/// ]
func getMonthOperationAndGroupByCategory(queryDate : Date = Date()) -> [RecordCategory:[String: Double]] {
    var dateComponents =  calendar.dateComponents([.year, .month, .day], from: queryDate)
    
    dateComponents.day = 1
    
    let qDate1 = calendar.date(from: dateComponents)! // day
    let qDate2 = calendar.date(byAdding: .month, value: 1, to: qDate1) // day + 1
    return groupByDateAndCategory(qDate1, qDate2)
}

/// [
//    RecordCategory.Food : ["Income": 20, "Outcome": 5, "Balance": 5]
/// ]
func getYearOperationAndGroupByCategory(queryDate : Date = Date()) -> [RecordCategory:[String: Double]] {
    var dateComponents =  calendar.dateComponents([.year, .month, .day], from: queryDate)
    
    dateComponents.month = 1
    dateComponents.day = 1
    
    let qDate1 = calendar.date(from: dateComponents)! // day
    let qDate2 = calendar.date(byAdding: .year, value: 1, to: qDate1) // day + 1
    return groupByDateAndCategory(qDate1, qDate2)
}

// 給定一段時間，回傳該段時間的 income, outcome, balance，將每個類別分開列出
func groupByDateAndCategory(_ qDate1: Date, _ qDate2: Date?) -> [RecordCategory:[String: Double]] {
    var group : [RecordCategory:[String: Double]] = [:]
    let realm = try! Realm()
    let results = realm.objects(Record.self).filter("date >= %@ AND date < %@", qDate1 as NSDate, qDate2! as NSDate)
    
    var entry = ["income": 0.0, "outcome": 0.0, "balance": 0.0]
    var lastCategory: RecordCategory?
    for i in results.sorted(byKeyPath: "category.name") {
        if i.category?.name != lastCategory?.name {
            if lastCategory != nil {
                group[lastCategory!] = entry
            }
            lastCategory = i.category
            entry = ["income": 0.0, "outcome": 0.0, "balance": 0.0]
        }
        if (i.category?.isIncome)! {
            entry["balance"]! += i.sum
            entry["income"]! += i.sum
        } else {
            entry["balance"]! -= i.sum
            entry["outcome"]! += i.sum
        }
    }
    if lastCategory != nil { // no result
        group[lastCategory!] = entry
    }
    return group
}
