//
//  ChartViewController.swift
//  Spending
//
//  Created by master on 2018/1/15.
//  Copyright © 2018年 test. All rights reserved.
//

import UIKit
import Charts
import QMUIKit

class ChartViewController: QMUICommonViewController, UIToolbarDelegate {
    var tabBarMode = false
    
    func position(for bar: UIBarPositioning) -> UIBarPosition {
        return .top
    }
    
    let toolBar = UIToolbar()
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        if isNeedUpdate {
            chartRecords = getYearOperationAndGroupByMonth()
            balanceChartView.setData(chartRecords)
        }
    }
    
    var isNeedUpdate = false
    @objc func recordChanged() {
        isNeedUpdate = true
    }
    
    let balanceChartView = BalanceChartView()
    var chartRecords = getWeekOperationAndGroupByDay()
    
    @objc func touchSegment(sender: UISegmentedControl) {
        switch  sender.selectedSegmentIndex {
        case 0:
            chartRecords = getWeekOperationAndGroupByDay()
        case 1:
            chartRecords = getMonthOperationAndGroupByDay()
        case 2:
            chartRecords = getYearOperationAndGroupByMonth()
        default:
            break
        }
        balanceChartView.setData(chartRecords)
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        balanceChartView.setData(chartRecords)
        NotificationCenter.default.addObserver(self, selector: #selector(recordChanged), name: UTRecordChanged, object: nil)
        
        toolBar.delegate = self
        toolBar.barTintColor = navigationController?.navigationBar.barTintColor
        view.addSubview(toolBar)
        toolBar.snp.makeConstraints { (make) in
            make.left.right.equalTo(view)
            make.top.equalTo(topLayoutGuide.snp.bottom)
            make.height.equalTo(40)
        }
        let tabBarContainerView = UIView()
        tabBarContainerView.snp.makeConstraints { (make) in
            make.width.equalTo(UIScreen.main.bounds.width)
            make.height.equalTo(50)
        }
        
        let segmentControl = UISegmentedControl(items: ["週".localized(), "月".localized(), "年".localized()])
        tabBarContainerView.addSubview(segmentControl)
        segmentControl.selectedSegmentIndex = 0
        segmentControl.tintColor = QDThemeManager.sharedInstance().currentTheme.segmentControlTintColor()
        segmentControl.addTarget(self, action: #selector(touchSegment), for: .valueChanged)
        segmentControl.snp.makeConstraints { (make) in
            make.left.equalTo(tabBarContainerView)
            make.right.equalTo(tabBarContainerView)
            make.centerX.equalTo(tabBarContainerView)
            make.top.equalTo(tabBarContainerView)
        }
        
        let barItem = UIBarButtonItem(customView: tabBarContainerView)
        toolBar.items = [barItem]


        view.backgroundColor = UIColor.white

        let scrollView = UIScrollView()
        view.addSubview(scrollView)
        scrollView.snp.makeConstraints { (make) in
            make.top.equalTo(toolBar.snp.bottom)
            make.left.right.equalTo(view)
            make.bottom.equalTo(bottomLayoutGuide.snp.bottom)
        }
        scrollView.autoresizingMask = [.flexibleWidth, .flexibleHeight]
        scrollView.showsVerticalScrollIndicator = false

        let scrollViewContentView = UIView()
        scrollView.addSubview(scrollViewContentView)
        
        scrollViewContentView.snp.makeConstraints { (make) in
            make.edges.equalTo(scrollView)
            make.width.equalTo(view)
        }
        
        let balanceChartTitleLabel = UILabel()
        scrollViewContentView.addSubview(balanceChartTitleLabel)
        balanceChartTitleLabel.text = "收支平衡圖".localized()
        balanceChartTitleLabel.font = UIFont.systemFont(ofSize: 15)
        balanceChartTitleLabel.textColor = UIColor.black
        balanceChartTitleLabel.snp.makeConstraints { (make) in
            make.centerX.equalTo(scrollViewContentView)
            make.top.equalTo(scrollViewContentView).offset(10)
        }
        
        scrollViewContentView.addSubview(balanceChartView)
        balanceChartView.snp.makeConstraints { (make) in
            make.top.equalTo(balanceChartTitleLabel.snp.bottom).offset(5)
            make.left.equalTo(scrollViewContentView).offset(20)
            make.right.equalTo(scrollViewContentView).offset(-20)
            make.height.equalTo(200)
            make.bottom.equalTo(scrollViewContentView)
        }
        
        let separatorLine = UIView()
        scrollViewContentView.addSubview(separatorLine)
        separatorLine.backgroundColor = UIColor.lightGray
        separatorLine.snp.makeConstraints { (make) in
            make.top.equalTo(balanceChartView.snp.bottom).offset(5)
            make.left.right.equalTo(scrollViewContentView)
            make.height.equalTo(0.5)
        }
    }
    
    @objc func balanceDetail() {
        present(DetailChartViewController(.balance), animated: true, completion: nil)
    }
    
    @objc func OpDetail() {
        present(DetailChartViewController(.op), animated: true, completion: nil)
    }
    
    @objc func pieChartDetail() {
        present(DetailChartViewController(.pie), animated: true, completion: nil)
    }
    
    @objc func close() {
        dismiss(animated: true, completion: nil)
    }
}
