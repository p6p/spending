//
//  ChartViewController.swift
//  Spending
//
//  Created by master on 2018/1/15.
//  Copyright © 2018年 test. All rights reserved.
//

import UIKit
import Charts
import RealmSwift
import QMUIKit

class ChartViewController1: UIViewController {
    let pieChartView = PieChartView()
    let realm = try! Realm()
    
    let calendar = Calendar.current
    var recordsDate = Date()
    
    // 將 record 按天分成不同 section
    func groupRecordByMonth() -> [[String: Double]] {
        var group : [[String: Double]] = []
        var dateComponents =  calendar.dateComponents([.year,.month, .day, .hour,.minute,.second], from: recordsDate)
        
        let range = calendar.range(of: .day, in: .month, for: recordsDate) // 該月份範圍
        for i in  1...(range?.count)! {
            dateComponents.day = i
            
            let qDate1 = calendar.date(from: dateComponents)! // day
            let qDate2 = calendar.date(byAdding: .day, value: 1, to: qDate1) // day + 1
            let results = realm.objects(Record.self).filter("date >= %@ AND date < %@", qDate1 as NSDate, qDate2! as NSDate)
            
            var balance = 0.0
            var income = 0.0
            var outcome = 0.0
            for i in results {
                if (i.category?.isIncome)! {
                    balance += i.sum
                    income += i.sum
                } else {
                    balance -= i.sum
                    outcome += i.sum
                }
            }
            let dict = ["balance" : balance, "income" : income, "outcome" : outcome]
            group.append(dict)
        }
        return group
    }
    
    // 將 record 按天分成不同 section
    func groupRecordByWeek() -> [[String: Double]] {
        var group : [[String: Double]] = []
        var dateComponents =  calendar.dateComponents([.year, .month, .weekOfMonth, .weekday], from: recordsDate)
        
        for i in  1...7 {
            dateComponents.weekday = i
            
            let qDate1 = calendar.date(from: dateComponents)! // day
            let qDate2 = calendar.date(byAdding: .day, value: 1, to: qDate1) // day + 1
            let results = realm.objects(Record.self).filter("date >= %@ AND date < %@", qDate1 as NSDate, qDate2! as NSDate)
            
            var balance = 0.0
            var income = 0.0
            var outcome = 0.0
            for i in results {
                if (i.category?.isIncome)! {
                    balance += i.sum
                    income += i.sum
                } else {
                    balance -= i.sum
                    outcome += i.sum
                }
            }
            let dict = ["balance" : balance, "income" : income, "outcome" : outcome]
            group.append(dict)
        }
        return group
    }
    
    // 將 record 按天分成不同 section
    func groupRecordByYear() -> [[String: Double]] {
        var group : [[String: Double]] = []
        var dateComponents =  calendar.dateComponents([.year,.month, .day], from: recordsDate)
        
        let range = calendar.range(of: .day, in: .month, for: recordsDate)
        for i in  1...12 {
            dateComponents.month = i
            dateComponents.day = 1
            let qDate1 = calendar.date(from: dateComponents)! // day
            let qDate2 = calendar.date(byAdding: .month, value: 1, to: qDate1)
            print("\(qDate1) \(qDate2)")
            let results = realm.objects(Record.self).filter("date >= %@ AND date < %@", qDate1 as NSDate, qDate2! as NSDate)
            
            var balance = 0.0
            var income = 0.0
            var outcome = 0.0
            for i in results {
                if (i.category?.isIncome)! {
                    balance += i.sum
                    income += i.sum
                } else {
                    balance -= i.sum
                    outcome += i.sum
                }
            }
            let dict = ["balance" : balance, "income" : income, "outcome" : outcome]
            group.append(dict)
        }
        return group
    }
    
    func testData() {
        var dateComponents =  calendar.dateComponents([.year,.month, .day, .hour,.minute,.second], from: recordsDate)
        for i in 1...31 {
            dateComponents.day = i
            let date = calendar.date(from: dateComponents)!
            let income1 = Record()
            income1.sum = 10.0 * Double(i)
            income1.date = date
            income1.category = realm.objects(RecordCategory.self).first
            try! realm.write {
                realm.add(income1)
            }
        }
        
    }
    
    
    let lineChartView = LineChartView()

    
    override func viewDidLoad() {
        super.viewDidLoad()
//        testData()
        let segControl = QMUISegmentedControl(items: ["一週", "一個月", "一年"])
        view.addSubview(segControl)
        segControl.selectedSegmentIndex = 0
        segControl.addTarget(self, action: #selector(touchSegControl), for: .valueChanged)
        segControl.translatesAutoresizingMaskIntoConstraints = false
        segControl.topAnchor.constraint(equalTo: view.topAnchor, constant: 20).isActive = true
        segControl.leadingAnchor.constraint(equalTo: view.leadingAnchor, constant: 0).isActive = true
        segControl.trailingAnchor.constraint(equalTo: view.trailingAnchor, constant: 0).isActive = true
        
        
        view.addSubview(lineChartView)
        lineChartView.translatesAutoresizingMaskIntoConstraints = false
        lineChartView.topAnchor.constraint(equalTo: view.topAnchor, constant: 60).isActive = true
        lineChartView.leadingAnchor.constraint(equalTo: view.leadingAnchor, constant: 20).isActive = true
        lineChartView.trailingAnchor.constraint(equalTo: view.trailingAnchor, constant: -20).isActive = true
        lineChartView.heightAnchor.constraint(equalToConstant: 200).isActive = true
    }
    
    @objc func touchSegControl(sender: UISegmentedControl) {
        switch sender.selectedSegmentIndex {
        case 0:
             balanceView(groupRecordByWeek())
        case 1:
            balanceView(groupRecordByMonth())
        case 2:
            balanceView(groupRecordByYear())
        default:
            break
        }
    }
    
//    fileprivate func barChart() {
//        let barChartView = BarChartView()
//        view.addSubview(barChartView)
//        barChartView.translatesAutoresizingMaskIntoConstraints = false
//        barChartView.topAnchor.constraint(equalTo: view.topAnchor, constant: 20).isActive = true
//        barChartView.leadingAnchor.constraint(equalTo: view.leadingAnchor, constant: 20).isActive = true
//        barChartView.trailingAnchor.constraint(equalTo: view.trailingAnchor, constant: -20).isActive = true
//        barChartView.heightAnchor.constraint(equalToConstant: 200).isActive = true
//
//        let monthArray = ["Jan", "Feb", "Mar", "Apr", "May", "Jun", "Jul", "Aug", "Sep", "Oct", "Nov", "Dec"]
//        var temperatureArray:[Double] = [20, 21, 22, 23, 24, 25, 26, 27, 28, 29 ,30, 31, 32]
//
//        //生成一個存放資料的陣列，型別是BarChartDataEntry.
//        var dataEntries: [BarChartDataEntry] = []
//
//        //實作一個迴圈，來存入每筆顯示的資料內容
//        for i in 0..<monthArray.count {
//            //需設定x, y座標分別需顯示什麼東西
//            let dataEntry = BarChartDataEntry(x:Double(i),  y:temperatureArray[i])
//            //最後把每次生成的dataEntry存入到dataEntries當中
//            dataEntries.append(dataEntry)
//        }
//        //透過BarChartDataSet設定我們要顯示的資料為何，以及圖表下方的label
//        let chartDataSet = BarChartDataSet(values: dataEntries, label: "Temperature per month")
//        //把整個dataset轉換成可以顯示的BarChartData
//        let charData = BarChartData(dataSet: chartDataSet)
//        //最後在指定剛剛連結的myView要顯示的資料為charData
//        barChartView.data = charData
//
//        barChartView.xAxis.valueFormatter = IndexAxisValueFormatter(values: monthArray)
//        barChartView.xAxis.granularity = 1
//    }
    
    fileprivate func balanceView(_ group : [[String: Double]]) {
        //        barChart()
//        groupRecordByMonth()
        
        let yearXAxisValue = ["Jan", "Feb", "Mar", "Apr", "May", "Jun", "Jul", "Aug", "Sep", "Oct", "Nov", "Dec"]
        let weekXAxisValue = ["Sun", "Mon", "Tue", "Wed", "Thu", "Fri", "Sat"]
        var monthXAxisValue : [String] = []
        

        // 設定資料
        var dataEntries: [BarChartDataEntry] = []

        for i in 0..<group.count {
            let dataEntry = BarChartDataEntry(x:Double(i),  y:Double(group[i]["balance"]!))
            dataEntries.append(dataEntry)
            monthXAxisValue.append("\(i+1)")
        }
        let chartDataSet = LineChartDataSet(values: dataEntries, label: "Balance")
        let charData = LineChartData(dataSet: chartDataSet)
        lineChartView.data = charData
        
        // 設定座標軸
        lineChartView.xAxis.granularity = 1
        lineChartView.xAxis.labelPosition = .bottom
        
        let ll = ChartLimitLine(limit: 100.0, label: "Budget")
        lineChartView.rightAxis.addLimitLine(ll)
        
        lineChartView.chartDescription?.text = ""
        lineChartView.doubleTapToZoomEnabled = false
        lineChartView.dragXEnabled = false
        lineChartView.dragYEnabled = false
        lineChartView.scaleXEnabled = false
        lineChartView.scaleYEnabled = false
        
        lineChartView.rightAxis.enabled = false
        let leftAxis = lineChartView.leftAxis
//        leftAxis.axisMinimum = 0.0
        lineChartView.chartDescription?.text = "Balance View"
        
        if group.count == 7 {
            lineChartView.xAxis.valueFormatter = IndexAxisValueFormatter(values: weekXAxisValue)
        } else if group.count == 12 {
            lineChartView.xAxis.valueFormatter = IndexAxisValueFormatter(values: yearXAxisValue)
        } else {
            lineChartView.xAxis.valueFormatter = IndexAxisValueFormatter(values: monthXAxisValue)
        }
    }

//
//    override func didReceiveMemoryWarning() {
//        super.didReceiveMemoryWarning()
//        // Dispose of any resources that can be recreated.
//    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
