//
//  BalanceView.swift
//  Spending
//
//  Created by master on 2018/1/18.
//  Copyright © 2018年 test. All rights reserved.
//

import UIKit
import Charts

class BalanceChartView: BarChartView {
    func setData(_ group: [[String : Double]]) {
        // 1. 設定圖表數據
        var dataEntries: [BarChartDataEntry] = [] // 代表多個 bar 的集合，為其中一組數據，一張圖表可以同時顯示多組數據
        
        for i in 0..<group.count {
            let xValue = Double(i)
            let yValue = Double(group[i]["balance"]!)
            let dataEntry = BarChartDataEntry(x:xValue,  y:yValue)
            dataEntries.append(dataEntry)
        }
        
        // 2. 將數據給資料集合
        let balanceDataSet = BarChartDataSet(values: dataEntries, label: "結餘".localized())
        let color = hexColor(0x3949AB)
        balanceDataSet.setColor(color)
        balanceDataSet.highlightEnabled = false // 點擊不發生變化
        balanceDataSet.drawValuesEnabled = false // 因為 bar 的數量很多，數據可能發生重疊

        // 3. 將資料集合賦值給 view
        data = BarChartData(dataSet: balanceDataSet) // 指定顯示一組數據
        
        // 設定做座標軸顯示名稱
        if group.count == 7 {
            let weekXAxisName = ["Sun", "Mon", "Tue", "Wed", "Thu", "Fri", "Sat", "Sat"].map { (str) -> String in
                return str.localized()
            }
            xAxis.valueFormatter = IndexAxisValueFormatter(values: weekXAxisName)
        } else if group.count == 12 {
            let yearXAxisName = ["Jan", "Feb", "Feb", "Mar", "Apr", "May", "Jun", "Jul", "Aug", "Sep", "Oct", "Dec"].map { (str) -> String in
                return str.localized()
            }
            xAxis.valueFormatter = IndexAxisValueFormatter(values: yearXAxisName)
        } else {
            var monthXAxisName : [String] = []
            for i in 0..<group.count {
                monthXAxisName.append("\(i+1)")
            }
            xAxis.valueFormatter = IndexAxisValueFormatter(values: monthXAxisName)
        }
    }
    convenience init() {
        self.init(frame: CGRect.zero)
    }
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        
        // x 軸美化
        xAxis.drawGridLinesEnabled = false
        xAxis.granularity = 0.01 // 最小變化量，在貨幣中為 0.01
        xAxis.labelPosition = .bottom
        
        // y 軸美化
        leftAxis.drawGridLinesEnabled = false
        rightAxis.enabled = false
        
        // 不使用此功能
        chartDescription?.text = ""
        
        // 顯示為靜態圖表，不可進行互動
        doubleTapToZoomEnabled = false
        dragXEnabled = false
        dragYEnabled = false
        scaleXEnabled = false
        scaleYEnabled = false
    }
    
    init(_ group : [[String: Double]]) {
        super.init(frame: CGRect.zero)
        setData(group)
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
}
