//
//  DetailChartViewController.swift
//  Spending
//
//  Created by master on 2018/1/17.
//  Copyright © 2018年 test. All rights reserved.
//

import UIKit
import QMUIKit

class DetailChartViewController: UIViewController {
    enum ChartType {
        case balance, op, pie
    }
    
    var balanceChartView : BalanceChartView?
    var opView : OperationChartView?
    var pieChartView : CategoryChartView?
    var chartType : ChartType
    var chartView = UIView()
    
    init(_ chartType : ChartType) {
        self.chartType = chartType
        super.init(nibName: nil, bundle: nil)
        switch chartType {
        case .balance:
            balanceChartView = BalanceChartView(getWeekOperationAndGroupByDay())
            chartView = balanceChartView!
        case .op:
            opView = OperationChartView(getWeekOperationAndGroupByDay())
            chartView = opView!
        case .pie:
            pieChartView = CategoryChartView(getWeekOperationAndGroupByCategory())
            chartView = pieChartView!
        }
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    

    override func viewDidLoad() {
        super.viewDidLoad()
        view.backgroundColor = UIColor.white
        
        let closeBtn = UIButton()
        closeBtn.setTitle("關閉".localized(), for: .normal)
        closeBtn.setTitleColor(UIColor.black, for: .normal)
        closeBtn.translatesAutoresizingMaskIntoConstraints = false
        view.addSubview(closeBtn)
        closeBtn.topAnchor.constraint(equalTo: view.topAnchor, constant: 20).isActive = true
        closeBtn.leadingAnchor.constraint(equalTo: view.leadingAnchor, constant: 20).isActive = true
        closeBtn.addTarget(self, action: #selector(close), for: .touchUpInside)
        
        let segControl = QMUISegmentedControl(items: [NSLocalizedString("一週", comment: "一週"), NSLocalizedString("一個月", comment: "一個月"), NSLocalizedString("一年", comment: "一年")])
        view.addSubview(segControl)
        segControl.selectedSegmentIndex = 0
        segControl.addTarget(self, action: #selector(touchSegControl), for: .valueChanged)
        segControl.translatesAutoresizingMaskIntoConstraints = false
        segControl.topAnchor.constraint(equalTo: closeBtn.bottomAnchor, constant: 10).isActive = true
        segControl.leadingAnchor.constraint(equalTo: view.leadingAnchor, constant: 20).isActive = true
        segControl.trailingAnchor.constraint(equalTo: view.trailingAnchor, constant: -20).isActive = true
        
        view.addSubview(chartView)
        chartView.translatesAutoresizingMaskIntoConstraints = false
        chartView.topAnchor.constraint(equalTo: segControl.bottomAnchor, constant: 5).isActive = true
        chartView.leadingAnchor.constraint(equalTo: view.leadingAnchor, constant: 20).isActive = true
        chartView.trailingAnchor.constraint(equalTo: view.trailingAnchor, constant: -20).isActive = true
        chartView.heightAnchor.constraint(equalToConstant: 200).isActive = true
        
    }
    
    @objc func touchSegControl(sender: UISegmentedControl) {
        switch sender.selectedSegmentIndex {
        case 0:
            switch chartType {
            case .balance:
                balanceChartView?.setData(getWeekOperationAndGroupByDay())
            case .op:
                opView?.setGroup(getWeekOperationAndGroupByDay())
            case .pie:
                pieChartView?.setGroup(getWeekOperationAndGroupByCategory())
            }
        case 1:
            switch chartType {
            case .balance:
                balanceChartView?.setData(getMonthOperationAndGroupByDay())
            case .op:
                opView?.setGroup(getMonthOperationAndGroupByDay())
            case .pie:
                pieChartView?.setGroup(getMonthOperationAndGroupByCategory())
            }
        case 2:
            switch chartType {
            case .balance:
                balanceChartView?.setData(getYearOperationAndGroupByMonth())
            case .op:
                opView?.setGroup(getYearOperationAndGroupByMonth())
            case .pie:
                pieChartView?.setGroup(getYearOperationAndGroupByCategory())
            }
        default:
            break
        }
    }


    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    @objc func close() {
        dismiss(animated: true, completion: nil)
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
