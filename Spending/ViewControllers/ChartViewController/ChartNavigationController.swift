//
//  ChartNavigationController.swift
//  Spending
//
//  Created by master on 2018/2/20.
//  Copyright © 2018年 test. All rights reserved.
//

import UIKit
import Localize_Swift
import QMUIKit

class ChartNavigationController: QMUINavigationController {
    override func didInitialized() {
        
    }

    override func viewDidLoad() {
        super.viewDidLoad()
        
        let chartViewController = ChartViewController()
        chartViewController.hidesBottomBarWhenPushed = false
        chartViewController.title = "圖表".localized()
        chartViewController.tabBarMode = true
        pushViewController(chartViewController, animated: false)
        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
