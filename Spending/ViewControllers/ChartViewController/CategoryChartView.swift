//
//  PieView.swift
//  Spending
//
//  Created by master on 2018/1/18.
//  Copyright © 2018年 test. All rights reserved.
//

import UIKit
import Charts

class CategoryChartView: PieChartView {

    func setGroup(_ group: [RecordCategory : [String : Double]]) {
        var dataEntries: [PieChartDataEntry] = []

        for (key, value) in group {
            if !key.isIncome {
                let outcome = Double(value["outcome"]!)
                let dataEntry = PieChartDataEntry(value: outcome, label: key.name)
                dataEntries.append(dataEntry)
            }
        }
        
        let pieChartDataSet = PieChartDataSet(values: dataEntries, label: "")
//        pieChartDataSet.drawValuesEnabled = false
        pieChartDataSet.yValuePosition = .outsideSlice
        pieChartDataSet.colors = [hexColor(0x4FC3F7), hexColor(0x81C784), hexColor(0xFFD54F), hexColor(0xFF8A65), hexColor(0xFFF176), hexColor(0x7986CB), hexColor(0xEF5350)]
        
//        pieChartDataSet.valueLineColor = hexColor(0x27ae60)
        chartDescription?.text = "圓餅圖".localized()
        let pieChartData = PieChartData(dataSet: pieChartDataSet)
        pieChartData.setValueTextColor(UIColor.clear)
        pieChartData.setValueFont(UIFont.boldSystemFont(ofSize: 12))
        pieChartData.setDrawValues(false)
        data = pieChartData
    }
    
    init(_ group : [RecordCategory:[String: Double]]) {
        super.init(frame: CGRect.zero)
        dragDecelerationEnabled = false
        rotationEnabled = false
        highlightPerTapEnabled = false
        setGroup(group)
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
}
