//
//  File.swift
//  Spending
//
//  Created by master on 2018/1/18.
//  Copyright © 2018年 test. All rights reserved.
//

import Foundation

protocol ChartViewProtocol {
    func setGroup(_ group: Any)
}
