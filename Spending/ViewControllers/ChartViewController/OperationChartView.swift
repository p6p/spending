//
//  OpView.swift
//  Spending
//
//  Created by master on 2018/1/18.
//  Copyright © 2018年 test. All rights reserved.
//

import UIKit
import Charts
import Localize_Swift

class OperationChartView: BarChartView {

    func setGroup(_ group: [[String : Double]]) {
        let yearXAxisValue = ["Jan", "Feb", "Feb", "Mar", "Apr", "May", "Jun", "Jul", "Aug", "Sep", "Oct", "Dec"].map { (str) -> String in
            return str.localized()
        }
        
        let weekXAxisValue = ["Sun", "Mon", "Tue", "Wed", "Thu", "Fri", "Sat", "Sat"].map { (str) -> String in
            return str.localized()
        }
        
        
        
        var monthXAxisValue : [String] = []
        
        // 設定資料
        var incomeDataEntries: [BarChartDataEntry] = []
        var outcomeDataEntries: [BarChartDataEntry] = []
        
        for i in 0..<group.count {
            let incomeEntry = BarChartDataEntry(x:Double(i),  y:Double(group[i]["income"]!))
            incomeDataEntries.append(incomeEntry)
            let outcomeEntry = BarChartDataEntry(x:Double(i),  y:Double(group[i]["outcome"]!))
            outcomeDataEntries.append(outcomeEntry)
            monthXAxisValue.append("\(i+1)")
        }
        let incomeDataSet = BarChartDataSet(values: incomeDataEntries, label: "收入".localized())
//        incomeDataSet.drawCircleHoleEnabled = false
        incomeDataSet.setColor(incomeColor)
//        incomeDataSet.circleColors = [incomeColor]
//        incomeDataSet.circleRadius = 4.0
//        incomeDataSet.lineWidth = 2.0
        incomeDataSet.highlightEnabled = false

        let outcomeDataSet = BarChartDataSet(values: outcomeDataEntries, label: "支出".localized())
        outcomeDataSet.setColor(outcomeColor)
//        outcomeDataSet.circleColors = [outcomeColor]
//        outcomeDataSet.drawCircleHoleEnabled = false
//        outcomeDataSet.circleRadius = 4.0
//        outcomeDataSet.lineWidth = 2.0
        outcomeDataSet.highlightEnabled = false


        let charData = BarChartData(dataSets: [incomeDataSet, outcomeDataSet])
        leftAxis.drawGridLinesEnabled = false
        xAxis.drawGridLinesEnabled = false
        data = charData
        
        // 設定座標軸
        xAxis.granularity = 1
        xAxis.labelPosition = .bottom
        
//        let ll = ChartLimitLine(limit: 100.0, label: "Budget")
//        rightAxis.addLimitLine(ll)
        
        chartDescription?.text = ""
        doubleTapToZoomEnabled = false
        dragXEnabled = false
        dragYEnabled = false
        scaleXEnabled = false
        scaleYEnabled = false
        
        rightAxis.enabled = false
        //        let leftAxis = leftAxis
        //        leftAxis.axisMinimum = 0.0
        chartDescription?.text = "收入支出圖".localized()
        
        if group.count == 7 {
            xAxis.valueFormatter = IndexAxisValueFormatter(values: weekXAxisValue)
        } else if group.count == 12 {
            xAxis.valueFormatter = IndexAxisValueFormatter(values: yearXAxisValue)
        } else {
            xAxis.valueFormatter = IndexAxisValueFormatter(values: monthXAxisValue)
        }
    }
    
    init(_ group : [[String: Double]]) {
        super.init(frame: CGRect.zero)
        
        setGroup(group)
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }

}
