//
//  BackupViewController.swift
//  Spending
//
//  Created by master on 2018/2/1.
//  Copyright © 2018年 test. All rights reserved.
//

import UIKit
import MessageUI

class BackupViewController: UIViewController, UIDocumentPickerDelegate {
    
    

    override func viewDidLoad() {
        super.viewDidLoad()
        view.backgroundColor = UIColor.white
        
        let exposeBtn = UIButton()
        view.addSubview(exposeBtn)
        exposeBtn.setTitle("導出", for: .normal)
        exposeBtn.setTitleColor(UIColor.red, for: .normal)
        exposeBtn.addTarget(self, action: #selector(touchExposeBtn), for: .touchUpInside)
        exposeBtn.snp.makeConstraints({ (make) in
            make.bottom.equalTo(view).offset(-100)
            make.centerX.equalTo(view)
        })
        
        
    }
    
    @objc func touchExposeBtn() {
        let docPath = NSSearchPathForDirectoriesInDomains(.documentDirectory, .userDomainMask, true)[0]
        let url = URL(fileURLWithPath: docPath).appendingPathComponent("default.realm")

        let docExportViewController: UIDocumentPickerViewController?
        if #available(iOS 11.0, *) {
            docExportViewController = UIDocumentPickerViewController(urls: [url], in: .exportToService)
        } else {
            docExportViewController = UIDocumentPickerViewController(url: url, in: .exportToService)
        }

        present(docExportViewController!, animated: true, completion: nil)
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
