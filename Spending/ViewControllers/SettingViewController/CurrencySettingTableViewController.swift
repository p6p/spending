//
//  CurrencySettingTableViewController.swift
//  Spending
//
//  Created by master on 2018/1/14.
//  Copyright © 2018年 test. All rights reserved.
//

import UIKit
import QMUIKit

let kCurrency = "currency"

class CurrencySettingTableViewController: UITableViewController {
    
    let usualCurrency = ["USD", "CNY", "EUR", "GBP", "CHF", "JPY", "CAD", "AUD", "NZD", "BRL", "RUB", "INR", "AED", "HKD", "TWD", "SGD", "LVL", "MXN", "ZAR"]
    init() {
        super.init(style: .grouped)
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    let kCell = "Cell"
    override func viewDidLoad() {
        super.viewDidLoad()
        title = "貨幣選擇".localized()
        tableView.register(QMUITableViewCell.self, forCellReuseIdentifier: kCell)
        // Uncomment the following line to preserve selection between presentations
        // self.clearsSelectionOnViewWillAppear = false

        // Uncomment the following line to display an Edit button in the navigation bar for this view controller.
        // self.navigationItem.rightBarButtonItem = self.editButtonItem
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

    // MARK: - Table view data source

    override func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }

    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return usualCurrency.count
    }

    let usualFlageImageName = ["usa", "china", "flag-of-europe", "great-britain", "switzerland", "japan", "canada", "australia-flag", "new-zealand", "brazil", "russian-federation", "india", "united-arab-emirates", "hong-kong", "taiwan", "singapore", "latvia", "mexico", "south-africa"]
    let rate = [
        "AUD": 1.2763,
        "BGN": 1.5759,
        "BRL": 3.2171,
        "CAD": 1.2886,
        "CHF": 0.93675,
        "CNY": 6.3281,
        "CZK": 20.45,
        "DKK": 6.0023,
        "EUR": 0.80574,
        "GBP": 0.71844,
        "HKD": 7.8332,
        "HRK": 5.9846,
        "HUF": 252.28,
        "IDR": 13722,
        "ILS": 3.457,
        "INR": 64.929,
        "ISK": 99.67,
        "JPY": 106.23,
        "KRW": 1061.1,
        "MXN": 18.666,
        "MYR": 3.8935,
        "NOK": 7.7455,
        "NZD": 1.3685,
        "PHP": 51.857,
        "PLN": 3.3692,
        "RON": 3.7546,
        "RUB": 56.297,
        "SEK": 8.2056,
        "SGD": 1.3154,
        "THB": 31.32,
        "TRY": 3.788,
        "ZAR": 11.719
    ]
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: kCell, for: indexPath) as! QMUITableViewCell
        let row = indexPath.row
        cell.textLabel?.text = usualCurrency[row]
        cell.imageView?.image = UIImage(named: usualFlageImageName[row])
//        cell.detailTextLabel?.text = "\(rate[usualCurrency[row]])"

        return cell
    }
    
    var checkRow = 0
    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        tableView.deselectRow(at: indexPath, animated: true)
        tableView.cellForRow(at: indexPath)?.accessoryType = .checkmark
        let row = indexPath.row
        let userDefaults = UserDefaults.standard
        userDefaults.set(usualCurrency[row], forKey: kCurrency)
        dismiss(animated: true, completion: nil)
    }
}
