//
//  ReminderSettingTimeCell.swift
//  Spending
//
//  Created by master on 2018/3/5.
//  Copyright © 2018年 test. All rights reserved.
//

import UIKit

class ReminderSettingTimeCell: UITableViewCell {
    let timeLabel = UILabel()
    override init(style: UITableViewCellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)
        
        let titleLabel = UILabel()
        addSubview(titleLabel)
        titleLabel.text = "提醒時間"
        titleLabel.snp.makeConstraints { (make) in
            make.centerY.equalTo(self)
            make.left.equalTo(self).offset(ConfigurationManger.sharedInstance.spacingHorizontal)
        }
        
        
        addSubview(timeLabel)
        
        timeLabel.snp.makeConstraints { (make) in
            make.right.equalTo(self).offset(-ConfigurationManger.sharedInstance.paddingHorizontal)
            make.centerY.equalTo(self)
        }
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    
    func set(recordDate: Date) {
        let dateComponent = Calendar.current.dateComponents([.year, .month, .day, .hour, .minute], from: recordDate)
        timeLabel.text = "\(dateComponent.hour!):\(dateComponent.minute!)"
    }
    
}

