//
//  NewCategoryViewController.swift
//  Spending
//
//  Created by master on 2018/1/22.
//  Copyright © 2018年 test. All rights reserved.
//

import UIKit
import RealmSwift
import QMUIKit

class NewCategoryViewController: UIViewController {
    
    let segment = QMUISegmentedControl(items: ["收入".localized(), "支出".localized()])
    let nameTextField = UITextField()
    let realm = try! Realm()

    override func viewDidLoad() {
        super.viewDidLoad()
        view.backgroundColor = UIColor.white
        
        navigationItem.rightBarButtonItem = UIBarButtonItem(barButtonSystemItem: .done, target: self, action: #selector(done))
        
        view.addSubview(segment)
        segment.selectedSegmentIndex = 0
        segment.translatesAutoresizingMaskIntoConstraints = false
        segment.topAnchor.constraint(equalTo: view.layoutMarginsGuide.topAnchor).isActive = true
        segment.leadingAnchor.constraint(equalTo: view.leadingAnchor, constant: 20).isActive = true
        segment.trailingAnchor.constraint(equalTo: view.trailingAnchor, constant: -20).isActive = true
        
        view.addSubview(nameTextField)
        nameTextField.placeholder = "類別名稱".localized()
        nameTextField.translatesAutoresizingMaskIntoConstraints = false
        nameTextField.topAnchor.constraint(equalTo: segment.bottomAnchor, constant: 20).isActive = true
        nameTextField.leadingAnchor.constraint(equalTo: view.leadingAnchor, constant: 20).isActive = true
        nameTextField.widthAnchor.constraint(equalToConstant: 200).isActive = true
    }
    
    @objc func done() {
        let category = RecordCategory()
        category.name = nameTextField.text!
        category.isIncome = segment.selectedSegmentIndex == 0
        category.iconName = "bank"
        try! realm.write {
            realm.add(category)
        }
        dismiss(animated: true, completion: nil)
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
