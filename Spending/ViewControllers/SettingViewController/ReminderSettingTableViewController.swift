//
//  ReminderSettingTableViewController.swift
//  Spending
//
//  Created by master on 2018/1/14.
//  Copyright © 2018年 test. All rights reserved.
//

import UIKit
import UserNotifications

let kIsReminderOn = "isReminderOn"

class ReminderSettingTableViewController: UITableViewController {
    let kReuseIdentifier = "reuseIdentifier"
    let userDefaults = UserDefaults.standard
    
    init() {
        super.init(style: .grouped)
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        title = "提醒".localized()
        
        tableView.register(UITableViewCell.self, forCellReuseIdentifier: kReuseIdentifier)
        tableView.register(ReminderSettingTimeCell.self, forCellReuseIdentifier: "ReminderSettingTimeCell")
        tableView.register(FullAddRecordDateTimePickerCell.self, forCellReuseIdentifier: "FullAddRecordDateTimePickerCell")
    }

    // MARK: - Table view data source

    override func numberOfSections(in tableView: UITableView) -> Int {
        return 2
    }

    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if section == 0 {
            return 1
        } else {
            return 1 + (isExpand ? 1 : 0)
        }
    }

    var dateTime = Date()
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {

        let section = indexPath.section
        let row = indexPath.row
        if isExpand && section == 1 && row == 1 {   // Expand cell
            let cell = tableView.dequeueReusableCell(withIdentifier: "FullAddRecordDateTimePickerCell", for: indexPath) as! FullAddRecordDateTimePickerCell
            cell.updateHandler = { newDate in
                self.dateTime = newDate
                let dateTimeCell = self.tableView.cellForRow(at: IndexPath(row: 0, section: 1)) as! ReminderSettingTimeCell
                dateTimeCell.set(recordDate: newDate)
                SettingModel.sharedInstance.set(reminderTime: newDate)
                self.cancel()
                self.start()
            }
            cell.set(mode: .time)
            return cell
        }
        
        if section == 0 && row == 0 {
            let cell = tableView.dequeueReusableCell(withIdentifier: kReuseIdentifier, for: indexPath)
            cell.textLabel?.text = "開啟提醒".localized()
            let reminderSwitch = UISwitch()
            reminderSwitch.isOn = SettingModel.sharedInstance.enableReminder()
            reminderSwitch.addTarget(self, action: #selector(reminderSwitchChange), for: .valueChanged)
            cell.accessoryView = reminderSwitch
            cell.selectionStyle = .none
            return cell
        } else if section == 1 && row == 0 {
            let cell = tableView.dequeueReusableCell(withIdentifier: "ReminderSettingTimeCell", for: indexPath) as! ReminderSettingTimeCell
            if let reminderTime = SettingModel.sharedInstance.reminderTime() {
                cell.set(recordDate: reminderTime)
            } else {
                cell.set(recordDate: Date())
                SettingModel.sharedInstance.set(reminderTime: Date())
            }
            return cell

        }
        fatalError()
    }
    
    var isExpand = false
    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let section = indexPath.section
        let row = indexPath.row
        if section == 1 && row == 0 {
            if isExpand {
                isExpand = !isExpand
                tableView.beginUpdates()
                tableView.deleteRows(at: [IndexPath(row: 1, section: 1)], with: .fade)
                tableView.endUpdates()
            } else {
                isExpand = !isExpand
                tableView.beginUpdates()
                tableView.insertRows(at: [IndexPath(row: 1, section: 1)], with: .fade)
                tableView.endUpdates()
            }
            tableView.deselectRow(at: indexPath, animated: true)
        }
    }
    
    override func tableView(_ tableView: UITableView, titleForFooterInSection section: Int) -> String? {
        if section == 0 {
            return NSLocalizedString("每日會提醒進行記帳", comment: "每日會提醒進行記帳")
        } else {
            return ""
        }
    }
    
    override func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        let section = indexPath.section
        let row = indexPath.row
        if isExpand && section == 1 && row == 1 {
            return 200
        } else {
            return super.tableView(tableView, heightForRowAt: indexPath)
        }
    }
    
    @objc func reminderSwitchChange(sender: UISwitch) {
        let isOn = sender.isOn
        SettingModel.sharedInstance.set(enableReminder: isOn)
        if isOn {
            start()
        } else {
            cancel()
        }
        
    }
    
    func start() {
        let content = UNMutableNotificationContent()
        content.title = NSLocalizedString("記帳提醒", comment: "記帳提醒")
        content.body = NSLocalizedString("記帳時間到了！快來記帳", comment: "記帳時間到了！快來記帳")

        var date = DateComponents()
        date.hour = 20
        
        let calendarTrigger = UNCalendarNotificationTrigger(dateMatching: date, repeats: true)

        let request = UNNotificationRequest(identifier: "notification1", content: content, trigger: calendarTrigger)
        UNUserNotificationCenter.current().add(request, withCompletionHandler: nil)
    }
    
    func cancel() {
        UNUserNotificationCenter.current().removeAllPendingNotificationRequests()
    }
}
