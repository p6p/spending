//
//  SettingTableViewController.swift
//  Spending
//
//  Created by master on 2017/12/26.
//  Copyright © 2017年 test. All rights reserved.
//

import UIKit
import QMUIKit

class SettingTableViewController: QMUICommonTableViewController {
    // MARK: - -------------Navigation-------------------
    override func shouldSetStatusBarStyleLight() -> Bool {
        return true
    }
    
    // MARK: - --------------------init-------------------
    init() {
        super.init(style: .grouped)
    }
    
    required init!(coder aDecoder: NSCoder!) {
        fatalError("init(coder:) has not been implemented")
    }
    
    // MARK: - -----------------ViewController Life-----------------
    override func viewDidLoad() {
        super.viewDidLoad()
        // MARK: navigation controller
        title = "更多".localized()
        
        // MARK: table view
        tableView.register(UITableViewCell.self, forCellReuseIdentifier: "UITableViewCell")
        tableView.register(SettingTableViewCell.self, forCellReuseIdentifier: kSettingTableViewCell)
    }
    
    // MARK: - --------------Table view data source-------------
    
    // section number
    override func numberOfSections(in tableView: UITableView) -> Int {
        return dataSource.count
    }
    
    // row number
    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return dataSource[section].count
    }
    
    // cell
    let dataSource = [["升級為 VIP"], ["開啟密碼鎖", "類別管理", "關於"]]
    let settingModel = SettingModel()
    let passwordSwitch = UISwitch()
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let section = indexPath.section
        let row = indexPath.row
        
        let cell = tableView.dequeueReusableCell(withIdentifier: "UITableViewCell", for: indexPath)

        let settingTitle = dataSource[section][row]
        cell.textLabel?.text = settingTitle.localized()

        if settingTitle == "開啟密碼鎖" {
            passwordSwitch.isOn = settingModel.enablePassword()
            passwordSwitch.addTarget(self, action: #selector(passwordSwitchChange), for: .valueChanged)
            cell.accessoryView = passwordSwitch
            cell.selectionStyle = .none
        } else {
            cell.accessoryType = .disclosureIndicator
        }
        
        return cell
    }
    
    // selection
    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let section = indexPath.section
        let row = indexPath.row
        let settingTitle = dataSource[section][row]
        
        if settingTitle == "升級為 VIP" {
            navigationController?.pushViewController(ProPageViewController(), animated: true)
        } else if settingTitle == "貨幣" {
            navigationController?.pushViewController(CurrencySettingTableViewController(), animated: true)
        } else if settingTitle == "類別管理" {
            let vc = CategorySelectTableViewController()
            vc.editMode = true
            navigationController?.pushViewController(vc, animated: true)
        } else if settingTitle == "提醒" {
            navigationController?.pushViewController(ReminderSettingTableViewController(), animated: true)
        } else if settingTitle == "關於" {
            navigationController?.pushViewController(AboutViewController(), animated: true)
        }
        tableView.deselectRow(at: indexPath, animated: true)
    }
    
    // MARK: - ------------btn event------------
    
    @objc func passwordSwitchChange(sender : UISwitch) {
        if sender.isOn {
            PasswordManger.sharedInstance.setNewPassword(in: self, completion: { (success) in
                if !success {
                    sender.isOn = false
                    dismiss(animated: true, completion: nil)
                }
            })
        } else {
            PasswordManger.sharedInstance.disablePassword()
        }
    }
    
}

