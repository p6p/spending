//
//  swift
//  Spending
//
//  Created by master on 2018/2/20.
//  Copyright © 2018年 test. All rights reserved.
//

import UIKit
import SnapKit
import Localize_Swift
import QMUIKit

class BalanceView: CardView {
    let overviewTitleLabe = UILabel()
    let inflowLabel = QMUILabel()
    let outflowLabel = QMUILabel()
    let balanceLabel = QMUILabel()
    let inflowTitleLabel = UILabel()
    let outflowTitleLabel = UILabel()
    let balanceTitleLabel = UILabel()
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        
        // balance
        // 內容在 ViewWillAppear
        backgroundColor = UIColor.white
        
        addSubview(overviewTitleLabe)
        overviewTitleLabe.text = "總覽".localized()
        overviewTitleLabe.font = UIFont.boldSystemFont(ofSize: 16)
        
        addSubview(inflowTitleLabel)
        inflowTitleLabel.text = "總收入".localized()
        
        addSubview(outflowTitleLabel)
        outflowTitleLabel.text = "總支出".localized()
        
        addSubview(balanceTitleLabel)
        balanceTitleLabel.text = "結餘".localized()
        
        addSubview(inflowLabel)
        inflowLabel.canPerformCopyAction = true
        inflowLabel.textColor = sumRed
        
        addSubview(outflowLabel)
        outflowLabel.canPerformCopyAction = true
        outflowLabel.textColor = sumGreen
        
        addSubview(balanceLabel)
        balanceLabel.canPerformCopyAction = true
        balanceLabel.textColor = UIColor.darkGray
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    override func layoutSubviews() {
        super.layoutSubviews()
        
        overviewTitleLabe.snp.makeConstraints { (make) in
            make.top.equalTo(self).offset(20)
            make.centerX.equalTo(self)
        }
        
        inflowTitleLabel.snp.makeConstraints { (make) in
            make.leading.equalTo(self).offset(10)
            make.top.equalTo(self).offset(60)
        }
        
        outflowTitleLabel.snp.makeConstraints { (make) in
            make.leading.equalTo(inflowTitleLabel)
            make.top.equalTo(inflowTitleLabel.snp.bottom).offset(10)
        }
        
        balanceTitleLabel.snp.makeConstraints { (make) in
            make.leading.equalTo(inflowTitleLabel)
            make.top.equalTo(outflowLabel.snp.bottom).offset(10)
        }
        
        inflowLabel.snp.makeConstraints { (make) in
            make.trailing.equalTo(self).offset(-10)
            make.top.equalTo(self).offset(60)
        }
        
        outflowLabel.snp.makeConstraints { (make) in
            make.trailing.equalTo(inflowLabel)
            make.top.equalTo(inflowLabel.snp.bottom).offset(10)
        }
        
        balanceLabel.snp.makeConstraints { (make) in
            make.trailing.equalTo(inflowLabel)
            make.top.equalTo(outflowLabel.snp.bottom).offset(10)
        }
    }

    /*
    // Only override draw() if you perform custom drawing.
    // An empty implementation adversely affects performance during animation.
    override func draw(_ rect: CGRect) {
        // Drawing code
    }
    */

}
