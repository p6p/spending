//
//  TodayRecordView.swift
//  Spending
//
//  Created by master on 2018/3/2.
//  Copyright © 2018年 test. All rights reserved.
//

import UIKit
import RealmSwift

class TodayViewController: UIViewController {
    let todayRecordView = TodayRecordView()
    let trTableViewController = TodayRecordTableViewController()

    override func viewDidLoad() {
        super.viewDidLoad()
        
        view.addSubview(todayRecordView)
        todayRecordView.snp.makeConstraints { (make) in
            make.edges.equalTo(view)
        }
        
        addChildViewController(trTableViewController)
        view.addSubview(trTableViewController.view)
        trTableViewController.didMove(toParentViewController: self)
        trTableViewController.view.snp.makeConstraints { (make) in
            make.edges.equalTo(view)
        }
        
    }
    
    override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()
        trTableViewController.view.snp.makeConstraints { (make) in
//            make.
        }
        
    }
}

class TodayRecordView: CardView {

    override init(frame: CGRect) {
        super.init(frame: frame)
        
    }

    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    override func layoutSubviews() {
        super.layoutSubviews()
    }
}

class TodayRecordTableViewController: UITableViewController {
    var recordsDate = Date()
    let realm = try! Realm()
    var todayRecord: Results<Record>?
    
    // 找到今天的紀錄
    func groupRecord() -> Results<Record> {
        var dateComponents =  calendar.dateComponents([.year,.month, .day, .hour,.minute,.second], from: recordsDate)
        dateComponents.hour = 0
        dateComponents.minute = 0
        dateComponents.second = 0
        let qDate1 = calendar.date(from: dateComponents)! // day
        let qDate2 = calendar.date(byAdding: .day, value: 1, to: qDate1) // day + 1
        let results = realm.objects(Record.self).filter("date >= %@ AND date < %@", qDate1 as NSDate, qDate2! as NSDate)
        return results
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        tableView.register(RecordListCell.self, forCellReuseIdentifier: "reuseIdentifier")
        
    }
    
    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        todayRecord = groupRecord()
        return todayRecord!.count
    }
    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "reuseIdentifier", for: indexPath) as! RecordListCell
        
        let indexNum = indexPath.row
        let section = indexPath.section
        
        let categoryName = todayRecord![indexNum].category!.name
        let categoryImageName = todayRecord![indexNum].category!.iconName
        let isIncome = todayRecord![indexNum].category!.isIncome
        let amount = todayRecord![indexNum].sum
        
        cell.set(categoryName: categoryName, categoryImageName: categoryImageName, isIncome: isIncome, amount: amount)
        
        return cell
    }
}
