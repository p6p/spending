//
//  HomeViewController.swift
//  Spending
//
//  Created by master on 2017/12/26.
//  Copyright © 2017年 test. All rights reserved.
//

import UIKit
import RealmSwift
import GoogleMobileAds
import Crashlytics
import Firebase
import FirebaseRemoteConfig
import SnapKit
import Localize_Swift
import QMUIKit
import MessageUI
import FSCalendar


func doubleToString(_ num: Double) -> String {
    return String(format: "%g", num)
}

class HomeViewController: QMUICommonViewController, FSCalendarDelegate, FSCalendarDataSource {
    let realm = try! Realm()
    var bannerView: GADBannerView!
    var interstitial: GADInterstitial!
    let listBtn = UIButton()
    let chartBtn = UIButton()
    
    var needShowBannerAd = true
    
    func updateAd() {
        if needShowBannerAd {
            bannerView = GADBannerView(adSize: kGADAdSizeBanner)
            #if DEBUG
                bannerView.adUnitID = "ca-app-pub-3940256099942544/2934735716"
            #else
                bannerView.adUnitID = "ca-app-pub-3953762937028581/1711983258"
            #endif
            
            view.addSubview(bannerView)
            bannerView.isHidden = false
            bannerView.translatesAutoresizingMaskIntoConstraints = false
            bannerView.centerXAnchor.constraint(equalTo: view.centerXAnchor).isActive = true
            bannerView.bottomAnchor.constraint(equalTo: bottomLayoutGuide.topAnchor).isActive = true
            bannerView.rootViewController = self
            bannerView.load(GADRequest())
        } else {
            if bannerView != nil {
                bannerView.removeFromSuperview()
                bannerView = nil
            }
        }
    }
    
    // config
    let cardBackgroundColor = UIColor.white
    var tabBarMode = false
    
    
    // MARK: - Navigation Setup
    override func setNavigationItemsIsInEditMode(_ isInEditMode: Bool, animated: Bool) {
        super.setNavigationItemsIsInEditMode(isInEditMode, animated: animated)
        title = "首頁".localized()
        
        // add Btn and minusBtn
        let addItem = UIBarButtonItem(barButtonSystemItem: .add, target: self, action: #selector(touchAddBtn))
        addItem.accessibilityIdentifier = "+"
        navigationItem.rightBarButtonItems = [addItem]
        
    }
    
    // MARK: - QMUINavigationControllerDelegate
    override func shouldSetStatusBarStyleLight() -> Bool {
        return true
    }
    
    override func didInitialized() {
        super.didInitialized()
    }
    
    let calendar = FSCalendar()
    let trTableViewController = TodayRecordTableViewController()
    override func initSubviews() {
        super.initSubviews()
        view.addSubview(calendar)
        calendar.dataSource = self
        calendar.delegate = self
//        calendar.scope = .week
        
        addChildViewController(trTableViewController)
        view.addSubview(trTableViewController.view)
        trTableViewController.didMove(toParentViewController: self)
        trTableViewController.view.snp.makeConstraints { (make) in
            make.left.right.equalTo(view)
            make.top.equalTo(calendar.snp.bottom)
            make.bottom.equalTo(bottomLayoutGuide.snp.top)
        }
    }
    
    override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()
        calendar.snp.makeConstraints { (make) in
            make.top.equalTo(topLayoutGuide.snp.bottom)
            make.left.right.equalTo(view)
            make.height.equalTo(300)
        }
    }
    
    func calendar(_ calendar: FSCalendar, boundingRectWillChange bounds: CGRect, animated: Bool) {
        calendar.snp.updateConstraints { (make) in
            make.height.equalTo(bounds.height)
        }
        view.layoutIfNeeded()
    }
    
//    func calendar(_ calendar: FSCalendar!, didSelect date: Date!) {
//
//    }
    
    func calendar(_ calendar: FSCalendar, didSelect date: Date, at monthPosition: FSCalendarMonthPosition) {
        trTableViewController.recordsDate = date
        trTableViewController.tableView.reloadData()
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // background color
        view.backgroundColor = QDThemeManager.sharedInstance().currentTheme.homeBackgroundColor()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        // balance 新增或修改，會造成 balance 改變，需要每次重新計算
        
        
        if UserManger.sharedInstance.getUserType() == .plus || UserManger.sharedInstance.getUserType() == .pro {
            needShowBannerAd = false
        } else {
            needShowBannerAd = true
        }
    }
    
    // MARK: - Btn event
    @objc func touchDebugBtn() {
        needShowBannerAd = !needShowBannerAd
        updateAd()
    }
    
    @objc func touchSettingBtn() {
        present(SettingNavigationController(), animated: true, completion: nil)
    }
    
    @objc func touchAddBtn() {
        let addRecordNavController = AddRecordNavigationController()
        addRecordNavController.category = realm.objects(RecordCategory.self).filter("isIncome = true").first
        present(addRecordNavController, animated: true, completion: nil)
//        if self.calendar.scope == .month {
//            self.calendar.setScope(.week, animated: true)
//        } else {
//            self.calendar.setScope(.month, animated: true)
//        }
    }
    
    @objc func touchMinusBtn() {
        let addRecordNavController = AddRecordNavigationController()
        addRecordNavController.category = realm.objects(RecordCategory.self).filter("isIncome = false").first
        present(addRecordNavController, animated: true, completion: nil)
    }
    
    @objc func touchFastOutcomeBtn(sender : UIButton) {
        let addRecordNavController = AddRecordNavigationController()
        let realm = try! Realm()
        let result = realm.objects(RecordCategory.self)
        var categories : [RecordCategory] = []
        // user default
        let userDefaults = UserDefaults.standard
        let fastRecord = userDefaults.object(forKey: "fastRecord") as! [Int]
        for i in fastRecord {
            categories.append(result[i])
        }
        addRecordNavController.category = categories[sender.tag]
        present(addRecordNavController, animated: true, completion: nil)
    }
}
