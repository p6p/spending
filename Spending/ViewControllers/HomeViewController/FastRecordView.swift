//
//  SwiftRecordswift
//  Spending
//
//  Created by master on 2018/2/23.
//  Copyright © 2018年 test. All rights reserved.
//

import UIKit
import QMUIKit
import SnapKit
import RealmSwift

class FastRecordView: UIView {
    let fStackView = UIStackView()
    let bgView = CardView()
    let rStackView = [UIStackView(), UIStackView()]
    let descriptLabel = UILabel()
    
    // viewDidLoad touchContentBtn
    var categories : [RecordCategory] = []

    override init(frame: CGRect) {
        super.init(frame: frame)
        
        // full stack view
        addSubview(fStackView)

        fStackView.alignment = .fill
        fStackView.spacing = 5
        fStackView.axis = .vertical
        fStackView.distribution = .fillEqually
        
        fStackView.translatesAutoresizingMaskIntoConstraints = false
        fStackView.snp.makeConstraints { (make) in
            make.leading.equalTo(self).offset(13)
            make.trailing.equalTo(self).offset(-13)
        }
        
        // stack view background color 與 stack view 位置大小相同
        fStackView.addSubview(bgView)
        bgView.backgroundColor = UIColor.white
        
        // 說明文字
        bgView.addSubview(descriptLabel)
        descriptLabel.text = "快速記帳".localized()
        descriptLabel.font = UIFont.boldSystemFont(ofSize: 16)
        
        // user default
        let userDefaults = UserDefaults.standard
        let fastRecord = userDefaults.object(forKey: "fastRecord") as! [Int]
        
        let realm = try! Realm()
        let result = realm.objects(RecordCategory.self)
        for i in fastRecord {
            categories.append(result[i])
        }
        
        let itemNumber = categories.count
        let itemInRow = 4
        let rowNumber = Int(ceil(Double(itemNumber) / Double(itemInRow)))
        
        for i in 0...rowNumber-1 {
            fStackView.addArrangedSubview(rStackView[i])
            rStackView[i].alignment = .fill
            rStackView[i].spacing = 5
            rStackView[i].distribution = .fillEqually
            
            for j in 0...itemInRow-1 {
                let index = i * 4 + j
                
                // 支出項目按鈕
                let fastOutcomeBtn = QMUIButton()
                rStackView[i].addArrangedSubview(fastOutcomeBtn)
                fastOutcomeBtn.tag = index
                fastOutcomeBtn.snp.makeConstraints({ (make) in
                    make.height.equalTo(65)
                })
                
                if index < itemNumber {
                    // 快速支出 icon
                    let iconContainer = UIView()
                    fastOutcomeBtn.addSubview(iconContainer)
                    iconContainer.isUserInteractionEnabled = false
                    iconContainer.layer.cornerRadius = 20
                    iconContainer.layer.borderColor = UIColor.darkGray.cgColor
                    iconContainer.layer.borderWidth = 2
                    iconContainer.snp.makeConstraints({ (make) in
                        make.width.height.equalTo(40)
                        make.top.centerX.equalTo(fastOutcomeBtn)
                    })
                    
                    let icon = UIImage(named: categories[index].iconName)?.qmui_imageResized(inLimitedSize: CGSize(width: 25, height: 25)).qmui_image(withTintColor: UIColor.darkGray)
                    let iconView = UIImageView(image: icon)
                    iconContainer.addSubview(iconView)
                    iconView.snp.makeConstraints({ (make) in
                        make.center.equalTo(iconContainer)
                    })
                    
                    // 快速支出 title label
                    let titleLabel = UILabel()
                    fastOutcomeBtn.addSubview(titleLabel)
                    titleLabel.text = categories[index].name
                    titleLabel.font = titleLabel.font.withSize(10)
                    titleLabel.snp.makeConstraints({ (make) in
                        make.top.equalTo(iconContainer.snp.bottom).offset(8)
                        make.centerX.equalTo(fastOutcomeBtn)
                    })
                }
            }
        }
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
//    override var intrinsicContentSize: CGSize {
//        return CGSize(width: 200, height: frame.height)
//    }
    
    override func layoutSubviews() {
        super.layoutSubviews()
        bgView.translatesAutoresizingMaskIntoConstraints = false
        bgView.snp.makeConstraints { (make) in
            make.top.equalTo(self)
            make.top.equalTo(fStackView).offset(-45)
            make.bottom.equalTo(fStackView).offset(10)
            make.leading.equalTo(self)
            make.trailing.equalTo(self)
        }
        
        descriptLabel.translatesAutoresizingMaskIntoConstraints = false
        descriptLabel.snp.makeConstraints { (make) in
            make.centerX.equalTo(bgView)
            make.top.equalTo(bgView).offset(15)
        }
    }
    
}
