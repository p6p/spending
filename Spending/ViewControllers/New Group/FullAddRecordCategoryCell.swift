//
//  FullAddRecordCategoryCell.swift
//  Spending
//
//  Created by master on 2018/3/5.
//  Copyright © 2018年 test. All rights reserved.
//

import UIKit

class FullAddRecordCategoryCell: UITableViewCell {
    let categoryBtn = UIButton()
    let categoryTitleLabel = UILabel()
    
    func set(categoryName: String, categoryImage: String) {
        categoryTitleLabel.text = categoryName
        categoryBtn.setImage(UIImage(named: categoryImage), for: .normal)
    }
    
    override init(style: UITableViewCellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)
        
        let titleLable = UILabel()
        addSubview(titleLable)
        titleLable.text = "類別"
        titleLable.snp.makeConstraints { (make) in
            make.centerY.equalTo(self)
            make.left.equalTo(self).offset(ConfigurationManger.sharedInstance.paddingHorizontal)
        }
        
        addSubview(categoryTitleLabel)
        addSubview(categoryBtn)
        categoryBtn.snp.makeConstraints { (make) in
            make.centerY.equalTo(self)
            make.right.equalTo(categoryTitleLabel.snp.left).offset(
                -ConfigurationManger.sharedInstance.spacingHorizontal)
        }
        
        categoryTitleLabel.snp.makeConstraints { (make) in
            make.centerY.equalTo(categoryBtn)
            make.right.equalTo(self).offset(
                -ConfigurationManger.sharedInstance.paddingHorizontal)
        }
    }
    
    static let cellHeight = 30
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    // MARK: - btn event
//    @objc func touchCategoryBtn() {
//        let vc = CategorySelectTableViewController()
//        vc.isIncome = (category?.isIncome)!
//        present(vc, animated: true, completion: nil)
//    }

}
