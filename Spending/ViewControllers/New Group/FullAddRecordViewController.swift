//
//  FullAddRecordViewController.swift
//  Spending
//
//  Created by master on 2018/3/5.
//  Copyright © 2018年 test. All rights reserved.
//

import UIKit
import SnapKit
import RealmSwift

class FullAddRecordViewController: UIViewController {
    let keyboardViewContorller = KeyboardViewController()
    
    var keyboardEnable = false {
        didSet {
            UIView.animate(withDuration: 0.3) {
                if self.keyboardEnable {
                    self.keyboardViewContorller.view.frame = CGRect(x: 0, y: self.view.frame.height - 250, width: self.view.frame.width, height: 250)
                } else {
                    self.keyboardViewContorller.view.frame = CGRect(x: 0, y: self.view.frame.height, width: self.view.frame.width, height: 250)
                }
            }
        }
    }

    let tableViewController = FullAddRecordTableViewController()
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // navigation
        navigationItem.rightBarButtonItem = UIBarButtonItem(barButtonSystemItem: .done, target: self, action: #selector(touchDone))
        
        navigationItem.leftBarButtonItem = UIBarButtonItem(barButtonSystemItem: .cancel, target: self, action: #selector(touchCancel))
        title = NSLocalizedString("新增紀錄", comment: "新增紀錄")

        addChildViewController(tableViewController)
        view.addSubview(tableViewController.view)
        tableViewController.didMove(toParentViewController: self)
        tableViewController.handleKeyboard = { show in
            if show == nil {
                self.keyboardEnable = !self.keyboardEnable
            } else {
                self.keyboardEnable = show!
            }
            
        }
        tableViewController.view.snp.makeConstraints { (make) in
            make.top.equalTo(topLayoutGuide.snp.bottom)
            make.left.right.equalTo(view)
            make.bottom.equalTo(bottomLayoutGuide.snp.top)
        }
        
        addChildViewController(keyboardViewContorller)
        view.addSubview(keyboardViewContorller.view)
        keyboardViewContorller.didMove(toParentViewController: self)
        keyboardViewContorller.updateHandler = tableViewController.keyboardUpdate
        if keyboardEnable {
            keyboardViewContorller.view.frame = CGRect(x: 0, y: view.frame.height - 250, width: view.frame.width, height: 250)
        } else {
            self.keyboardViewContorller.view.frame = CGRect(x: 0, y: self.view.frame.height, width: self.view.frame.width, height: 250)
        }
        
        
    }
    
    @objc func touchCancel() {
        dismiss(animated: true, completion: nil)
    }
    
    @objc func touchDone() {
        let realm = try! Realm()
        let record = Record()
        record.category = tableViewController.category
        record.date = tableViewController.dateTime
        record.sum = tableViewController.amount
        record.detail = tableViewController.getComment()
        try! realm.write {
            realm.add(record)
        }
    }


}
