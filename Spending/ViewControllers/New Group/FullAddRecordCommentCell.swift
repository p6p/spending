//
//  FullAddRecordCommentCell.swift
//  Spending
//
//  Created by master on 2018/3/5.
//  Copyright © 2018年 test. All rights reserved.
//

import UIKit

class FullAddRecordCommentCell: UITableViewCell, UITextFieldDelegate {
    let textField = UITextField()

    func get() -> String {
        return textField.text!
    }
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        return textField.resignFirstResponder()
    }
    
    func textFieldShouldBeginEditing(_ textField: UITextField) -> Bool {
        delegate?()
        return true
    }
    
    var delegate: (()->Void)?

    override init(style: UITableViewCellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)
        selectionStyle = .none
        addSubview(textField)
        textField.placeholder = "備註"
        textField.delegate = self
        textField.snp.makeConstraints { (make) in
            make.centerY.equalTo(self)
            make.left.equalTo(self).offset(ConfigurationManger.sharedInstance.paddingHorizontal)
            make.right.equalTo(self).offset(-ConfigurationManger.sharedInstance.paddingHorizontal)
        }
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    
    func set() {
        
    }

}
