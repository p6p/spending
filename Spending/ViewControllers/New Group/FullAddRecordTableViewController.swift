//
//  FullAddRecordTableViewController.swift
//  Spending
//
//  Created by master on 2018/3/5.
//  Copyright © 2018年 test. All rights reserved.
//

import UIKit
import RealmSwift
import QMUIKit

class FullAddRecordTableViewController: QMUICommonTableViewController, CategorySelectDelegate {
    
    fileprivate func updateNewCategory() {
        let cell = tableView.cellForRow(at: IndexPath(row: 0, section: 0)) as! FullAddRecordCategoryCell
        cell.set(categoryName: category!.name, categoryImage: category!.iconName)
    }
    
    init() {
        super.init(style: .grouped)
    }
    
    func getComment() -> String {
        let commentCell = tableView.cellForRow(at: IndexPath(row: (isExpand ? 4: 3), section: 0)) as! FullAddRecordCommentCell
        return commentCell.get()
    }
    
    required init!(coder aDecoder: NSCoder!) {
        fatalError("init(coder:) has not been implemented")
    }
    
    func selectedNewCategory(newCategory: RecordCategory) {
        category = newCategory
        updateNewCategory()
    }
    
    var amount = 0.0
    func keyboardUpdate(result: String) {
        amount = Double(result)!
        let cell = tableView.cellForRow(at: IndexPath(row: 1, section: 0)) as! FullAddRecordAmountCell
        cell.set(currencySymbol: "NT", amount: amount)
    }
    
    var handleKeyboard: ((Bool?)->Void)?
    
    var category: RecordCategory?

    override func viewDidLoad() {
        super.viewDidLoad()
        
        tableView.register(FullAddRecordCategoryCell.self, forCellReuseIdentifier: "FullAddRecordCategoryCell")
        tableView.register(FullAddRecordAmountCell.self, forCellReuseIdentifier: "FullAddRecordAmountCell")
        tableView.register(FullAddRecordDateTimeCell.self, forCellReuseIdentifier: "FullAddRecordDateTimeCell")
        tableView.register(FullAddRecordDateTimePickerCell.self, forCellReuseIdentifier: "FullAddRecordDateTimePickerCell")
        tableView.register(FullAddRecordCommentCell.self, forCellReuseIdentifier: "FullAddRecordCommentCell")
        // Do any additional setup after loading the view.
        let realm = try! Realm()
        category = realm.objects(RecordCategory.self)[0]
        
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 4 + (isExpand ? 1 : 0)
    }
    
    var isExpand = false
    var dateTime = Date()
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let row = indexPath.row
        
        if isExpand && row == 3 {   // Expand cell
            let cell = tableView.dequeueReusableCell(withIdentifier: "FullAddRecordDateTimePickerCell", for: indexPath) as! FullAddRecordDateTimePickerCell
            cell.updateHandler = { newDate in
                self.dateTime = newDate
                let dateTimeCell = self.tableView.cellForRow(at: IndexPath(row: 2, section: 0)) as! FullAddRecordDateTimeCell
                dateTimeCell.set(recordDate: newDate)
            }
            return cell
        }
        
        switch row {
        case 0:
            let cell = tableView.dequeueReusableCell(withIdentifier: "FullAddRecordCategoryCell", for: indexPath) as! FullAddRecordCategoryCell
            cell.set(categoryName: category!.name, categoryImage: category!.iconName)
            return cell
        case 1:
            let cell = tableView.dequeueReusableCell(withIdentifier: "FullAddRecordAmountCell", for: indexPath) as! FullAddRecordAmountCell
            cell.set(currencySymbol: "NT", amount: 0)
            return cell
        case 2:
            let cell = tableView.dequeueReusableCell(withIdentifier: "FullAddRecordDateTimeCell", for: indexPath) as! FullAddRecordDateTimeCell
            cell.set(recordDate: dateTime)
            return cell
        case 3:
            let cell = tableView.dequeueReusableCell(withIdentifier: "FullAddRecordCommentCell", for: indexPath) as! FullAddRecordCommentCell
            cell.delegate = {
                self.handleKeyboard!(false)
            }
            cell.set()
            return cell
        default:
            break
        }
        
        return UITableViewCell()
    }
    
    override func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        let row = indexPath.row
        if isExpand && row == 3 {   // Expand cell
            return FullAddRecordDateTimePickerCell.cellHeight
        }
        return super.tableView(tableView, heightForRowAt: indexPath)
    }
    
    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        tableView.deselectRow(at: indexPath, animated: true)
        let row = indexPath.row
        
        switch row {
        case 0:
            let vc = CategorySelectNavigationController()
            vc.selectDelegate = self
//            vc.isIncome = category!.isIncome
            present(vc, animated: true, completion: nil)
        case 1:
            handleKeyboard!(nil)
            view.endEditing(true)
        case 2:
            if isExpand {
                isExpand = !isExpand
                tableView.beginUpdates()
                tableView.deleteRows(at: [IndexPath(row: 3, section: 0)], with: .fade)
                tableView.endUpdates()
            } else {
                isExpand = !isExpand
                tableView.beginUpdates()
                tableView.insertRows(at: [IndexPath(row: 3, section: 0)], with: .fade)
                tableView.endUpdates()
            }
        default:
            break
        }
    }
    
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
