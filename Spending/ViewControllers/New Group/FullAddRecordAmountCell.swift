//
//  FullAmountCell.swift
//  Spending
//
//  Created by master on 2018/3/5.
//  Copyright © 2018年 test. All rights reserved.
//

import UIKit

class FullAddRecordAmountCell: UITableViewCell {
    let amountLabel = UILabel()
    let currencyLabel = UILabel()

    override init(style: UITableViewCellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)
        
        // currency
        addSubview(currencyLabel)
        addSubview(amountLabel)
//        currencyLabel.font = UIFont.systemFont(ofSize: 24)
//        currencyLabel.textColor = UIColor.black
        currencyLabel.snp.makeConstraints { (make) in
            make.centerY.equalTo(self)
            make.right.equalTo(amountLabel.snp.left)
        }
        
        // amountLabel
//        amountLabel.font = UIFont(name: "AvenirNext-Regular", size: 30)
//        amountLabel.textColor = U
//        amountLabel.textAlignment = .right
//        amountLabel.adjustsFontSizeToFitWidth = true
        amountLabel.snp.makeConstraints { (make) in
            make.centerY.equalTo(currencyLabel)
            make.right.equalTo(self).offset(-ConfigurationManger.sharedInstance.paddingHorizontal)
        }
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    // currencySymbol 像是 NT 或 $
    func set(currencySymbol: String, amount: Double) {
        currencyLabel.text = currencySymbol
        amountLabel.text = "\(amount)"
    }
}
