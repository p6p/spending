//
//  FullAddRecordDateTimeCell.swift
//  Spending
//
//  Created by master on 2018/3/5.
//  Copyright © 2018年 test. All rights reserved.
//

import UIKit

class FullAddRecordDateTimeCell: UITableViewCell {
    let dateLabel = UILabel()
    let timeLabel = UILabel()
    override init(style: UITableViewCellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)
        
        let titleLabel = UILabel()
        addSubview(titleLabel)
        titleLabel.text = "紀錄時間"
        titleLabel.snp.makeConstraints { (make) in
            make.centerY.equalTo(self)
            make.left.equalTo(self).offset(ConfigurationManger.sharedInstance.spacingHorizontal)
        }
        
        
        addSubview(dateLabel)
        addSubview(timeLabel)
        dateLabel.snp.makeConstraints { (make) in
            make.right.equalTo(timeLabel.snp.left).offset(-ConfigurationManger.sharedInstance.spacingHorizontal)
            make.centerY.equalTo(self)
        }
        
        timeLabel.snp.makeConstraints { (make) in
            make.right.equalTo(self).offset(-ConfigurationManger.sharedInstance.paddingHorizontal)
            make.centerY.equalTo(self)
        }
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    
    func set(recordDate: Date) {
        let dateComponent = Calendar.current.dateComponents([.year, .month, .day, .hour, .minute], from: recordDate)
        dateLabel.text = "\(dateComponent.month!)/\(dateComponent.day!)"
        timeLabel.text = "\(dateComponent.hour!):\(dateComponent.minute!)"
    }

}
