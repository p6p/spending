//
//  FullAddRecordDateTimePickerCell.swift
//  Spending
//
//  Created by master on 2018/3/6.
//  Copyright © 2018年 test. All rights reserved.
//

import UIKit

class FullAddRecordDateTimePickerCell: UITableViewCell {
    
    var updateHandler: ((Date)->Void)?
    
    @objc func valueChanged(sender: UIDatePicker) {
        updateHandler?(sender.date)
    }

    let picker = UIDatePicker()
    override init(style: UITableViewCellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)
        
        addSubview(picker)
        picker.datePickerMode = .dateAndTime
        picker.addTarget(self, action: #selector(valueChanged), for: .valueChanged)
        
        picker.snp.makeConstraints { (make) in
            make.center.equalTo(self)
        }
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    static let cellHeight: CGFloat = 200
    
    func set(mode: UIDatePickerMode) {
        picker.datePickerMode = mode
    }

}
