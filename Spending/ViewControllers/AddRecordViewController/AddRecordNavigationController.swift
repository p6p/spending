//
//  AddRecordNavigationController.swift
//  Spending
//
//  Created by master on 2017/12/31.
//  Copyright © 2017年 test. All rights reserved.
//

import UIKit
import QMUIKit

class AddRecordNavigationController: QMUINavigationController {
    var category : RecordCategory?
    
    init() {
        super.init(rootViewController: FullAddRecordViewController())
    }
    
    init(record : Record) {
        let vc = AddRecordViewController()
        vc.updateMode = true
        vc.record = record
        super.init(rootViewController: vc)
    }
    
    override init(nibName nibNameOrNil: String?, bundle nibBundleOrNil: Bundle?) {
        super.init(nibName: nibNameOrNil, bundle: nibBundleOrNil)
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
 }
