//
//  ReminderSettingTableViewController.swift
//  Spending
//
//  Created by master on 2018/1/14.
//  Copyright © 2018年 test. All rights reserved.
//

import UIKit

class AddRecordViewDetailController: UITableViewController {
    let kReuseIdentifier = "reuseIdentifier"
    
    init() {
        super.init(style: .grouped)
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        tableView.register(UITableViewCell.self, forCellReuseIdentifier: kReuseIdentifier)
        tableView.allowsSelection = false
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

    // MARK: - Table view data source

    override func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }

    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 2
    }
    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: kReuseIdentifier, for: indexPath)

        let row = indexPath.row
        if row == 0 {
            let dateFormatter = DateFormatter()
            dateFormatter.dateFormat = "yyyy-MM-dd hh:mm"
            cell.textLabel?.text = dateFormatter.string(from: Date())
        } else if row == 1 {
            let detailField = UITextField()
            cell.addSubview(detailField)
            detailField.placeholder = NSLocalizedString("項目", comment: "項目")
            detailField.clearButtonMode = .whileEditing
            detailField.translatesAutoresizingMaskIntoConstraints = false
            detailField.centerYAnchor.constraint(equalTo: cell.centerYAnchor).isActive = true
            detailField.leadingAnchor.constraint(equalTo: cell.leadingAnchor, constant: 12).isActive = true
            detailField.trailingAnchor.constraint(equalTo: cell.trailingAnchor).isActive = true
        }
        return cell
    }
    
    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let row = indexPath.row
        if row == 0 {
            tableView.deselectRow(at: indexPath, animated: true)
            
        }
    }

}
