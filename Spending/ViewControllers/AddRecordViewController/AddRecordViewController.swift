//
//  AddRecordViewController.swift
//  Spending
//
//  Created by master on 2017/12/31.
//  Copyright © 2017年 test. All rights reserved.
//

import UIKit
import RealmSwift
import QMUIKit

let UTRecordChanged = NSNotification.Name("UTRecordChanged") // 新增或移除紀錄

class AddRecordViewController: QMUICommonViewController {
    // MARK: - QMUINavigationControllerDelegate
    override func shouldSetStatusBarStyleLight() -> Bool {
        return true
    }
    
    var updateMode = false
    var record : Record?
    let sumLabel = UILabel()
    let kVC = KeyboardViewController()
    
    var category : RecordCategory?
    let categoryBtn = UIButton()
    let categoryTitleLabel = UILabel()
    let dateBtn = UIButton()
    
    let userDefaults = UserDefaults.standard
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        // 更新類別資訊
        categoryBtn.setImage(UIImage(named: category!.iconName), for: .normal)
        categoryTitleLabel.text = category!.name
        if category!.isIncome {
            view.backgroundColor = QDThemeManager.sharedInstance().currentTheme.addIncomeRecordBackgroundColor()
        } else {
            view.backgroundColor = QDThemeManager.sharedInstance().currentTheme.addOutcomeRecordBackgroundColor()
        }
//        dateBtn.setTitle("1 月 6 日", for: .normal)
    }
    

    override func viewDidLoad() {
        super.viewDidLoad()
        
        // background color
        view.backgroundColor = UIColor.white
        
        // keyboard view controller
        addChildViewController(kVC)
        view.addSubview(kVC.view)
        kVC.didMove(toParentViewController: self)
        kVC.view.translatesAutoresizingMaskIntoConstraints = false
        kVC.view.leadingAnchor.constraint(equalTo: view.leadingAnchor).isActive = true
        kVC.view.trailingAnchor.constraint(equalTo: view.trailingAnchor).isActive = true
        kVC.view.bottomAnchor.constraint(equalTo: view.bottomAnchor).isActive = true
        kVC.view.heightAnchor.constraint(equalToConstant: 250).isActive = true
        kVC.aVC = self
        
        // 設定模式
        let navController = navigationController as! AddRecordNavigationController
        if updateMode {
            title = NSLocalizedString("修改紀錄", comment: "修改紀錄")
            category = record?.category
            sumLabel.text = "\(doubleToString(record!.sum))"
            kVC.result = sumLabel.text!
        } else {
            title = NSLocalizedString("新增紀錄", comment: "新增紀錄")
            sumLabel.text = "0"
            category = navController.category
            kVC.result = "0"
//            category = (try! Realm()).objects(RecordCategory.self).first
        }
        let textColor = QDThemeManager.sharedInstance().currentTheme.addRecordTextColor()
        view.addSubview(sumLabel) // for auto layout
        
        // date btn
        view.addSubview(dateBtn)
        dateBtn.titleLabel?.font = UIFont.systemFont(ofSize: 24)
        dateBtn.setTitleColor(textColor, for: .normal)
        dateBtn.translatesAutoresizingMaskIntoConstraints = false
        dateBtn.topAnchor.constraint(equalTo: view.topAnchor, constant: 80).isActive = true
        dateBtn.leadingAnchor.constraint(equalTo: view.leadingAnchor, constant: 20).isActive = true
        
        
        
        // currency
        let currencyLabel = UILabel()
        view.addSubview(currencyLabel)
        currencyLabel.text = userDefaults.string(forKey: kCurrency)
        currencyLabel.font = sumLabel.font.withSize(24)
        currencyLabel.textColor = textColor
        currencyLabel.snp.makeConstraints { (make) in
            make.centerY.equalTo(sumLabel)
        }
        
        
        // sumLabel
        sumLabel.font = UIFont(name: "AvenirNext-Regular", size: 60)
        sumLabel.textColor = textColor
        sumLabel.textAlignment = .right
        sumLabel.adjustsFontSizeToFitWidth = true
        sumLabel.snp.makeConstraints { (make) in
            make.right.equalTo(view).offset(-ConfigurationManger.sharedInstance.paddingHorizontal)
            make.left.equalTo(currencyLabel.snp.right).offset(ConfigurationManger.sharedInstance.spacingHorizontal)
            make.bottom.equalTo(kVC.view.snp.top).offset(-20)
        }
        
        
        
        
        // navigation
        navigationItem.rightBarButtonItem = UIBarButtonItem(barButtonSystemItem: .done, target: self, action: #selector(touchDone))
        
        navigationItem.leftBarButtonItem = UIBarButtonItem(barButtonSystemItem: .cancel, target: self, action: #selector(touchCancel))
        
        // category btn
        view.addSubview(categoryBtn)
        categoryBtn.addTarget(self, action: #selector(touchCategoryBtn), for: .touchUpInside)
        categoryBtn.snp.makeConstraints { (make) in
            make.top.equalTo(dateBtn.snp.bottom).offset(10)
            make.left.equalTo(view).offset(20)
        }
        
        // category title label
        view.addSubview(categoryTitleLabel)
        categoryTitleLabel.textColor = textColor
        categoryTitleLabel.snp.makeConstraints { (make) in
            make.centerY.equalTo(categoryTitleLabel)
            make.left.equalTo(categoryBtn.snp.right).offset(10)
        }
        
//        // memo text field
//        let memoTextField = UITextField()
//        view.addSubview(memoTextField)
//        memoTextField.
    }
    
    // btn event
    @objc func touchCancel() {
        dismiss(animated: true, completion: nil)
    }

    @objc func touchDone() {
        let realm = try! Realm()
        if updateMode {
            try! realm.write {
                record!.category = category
                record!.date = Date()
                record!.sum = Double(sumLabel.text!)!
                record!.detail = ""
            }
            // ListViewController 對新數值進行更新
            let mainTabViewController = navigationController?.presentingViewController as! MainTabBarController
            let recordListNavigationController = mainTabViewController.selectedViewController as! RecordListNavigationController
            let recordListViewController = recordListNavigationController.viewControllers.first as! RecordListViewController
            recordListViewController.recordTableViewController.tableView.reloadData()
        } else {
            let record = Record()
            record.category = category
            record.date = Date()
            record.sum = Double(sumLabel.text!)!
            record.detail = ""
            try! realm.write {
                realm.add(record)
            }
        }
        
        // 新增或修改將對現有數據產生影響，需要通知 RecordList 和 ChartView 進行更新
        NotificationCenter.default.post(name: UTRecordChanged, object: nil)

        let vc = presentingViewController
        dismiss(animated: true) {
            QMUITips.showSucceed("新增成功".localized(), in: vc!.view, hideAfterDelay: 1)
        }
    }
    
    @objc func touchCategoryBtn() {
        let vc = CategorySelectTableViewController()
        vc.isIncome = (category?.isIncome)!
        present(vc, animated: true, completion: nil)
    }
    
    // keyboard
    func update() {
        sumLabel.text = kVC.result
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
}
