//
//  KeyBoardViewController.swift
//  Spending
//
//  Created by master on 2017/12/26.
//  Copyright © 2017年 test. All rights reserved.
//

import UIKit
import QMUIKit

class KeyboardViewController: UIViewController {
    // 給外面引用
    var result = "0"
    
    // viewDidLoad touchBtn
    var aVC : AddRecordViewController?
    var updateHandler: ((_ result: String)->Void)?

    override func viewDidLoad() {
        super.viewDidLoad()

        // keyboard pattarn
        let text = ["1", "2", "3"
                   ,"4", "5", "6"
                   ,"7", "8", "9"
                   ,".", "0", "⌫"
                   ]
        
        let fStack = UIStackView()
        view.addSubview(fStack)
        fStack.axis = .vertical
        fStack.distribution = .fillEqually
        fStack.translatesAutoresizingMaskIntoConstraints = false
        fStack.leadingAnchor.constraint(equalTo: view.leadingAnchor).isActive = true
        fStack.trailingAnchor.constraint(equalTo: view.trailingAnchor).isActive = true
        fStack.bottomAnchor.constraint(equalTo: view.bottomAnchor).isActive = true
        fStack.topAnchor.constraint(equalTo: view.topAnchor).isActive = true

        for i in 0...3 {
            // row stack view
            let rStack = UIStackView()
            fStack.addArrangedSubview(rStack)
            rStack.distribution = .fillEqually
            rStack.axis = .horizontal
            for j in 0...2 {
                // button
                let btn = UIButton()
                rStack.addArrangedSubview(btn)
                btn.backgroundColor = keyboardGray
                btn.titleLabel?.font = UIFont(name: "AvenirNext-Regular", size: 28)
                btn.setTitle("\(text[i*3+j])", for: .normal)
                btn.setTitleColor(UIColor(white: 1.0, alpha: 0.8), for: .normal)
                btn.layer.borderColor = UIColor.black.cgColor
//                btn.layer.borderWidth = 0.5
                btn.layer.masksToBounds = true
//                btn.layer.cornerRadius = 5
                btn.addTarget(self, action: #selector(touchBtn), for: .touchUpInside)
            }
        }
    }
    
    @objc func touchBtn(sender: UIButton) {
        let senderText = sender.titleLabel!.text!
        switch senderText {
        case "⌫": // 退格
            if Int(Double(result)!) > 10 {
                result = String(result.dropLast())
            } else {
                result = "0"
            }
            
        case "0", "1", "2", "3", "4", "5", "6", "7", "8", "9":
            if result == "0" {
                result = senderText
            } else {
                result += senderText
            }
            
        case ".":
            if result.range(of:".") == nil {
                result += "."
            } else {
                
            }
            
        default:
            break
        }
        
        // 更新顯示
        aVC?.update()
        updateHandler?(result)
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

}
