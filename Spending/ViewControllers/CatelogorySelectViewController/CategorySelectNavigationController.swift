//
//  CategorySelectNavigationController.swift
//  Spending
//
//  Created by master on 2018/3/6.
//  Copyright © 2018年 test. All rights reserved.
//

import UIKit

class CategorySelectNavigationController: UINavigationController {
    
    var selectDelegate: CategorySelectDelegate?

    override func viewDidLoad() {
        super.viewDidLoad()
        let categorySelectViewController = CategorySelectViewController()
        categorySelectViewController.delegate = selectDelegate
        title = "類別選擇"
        pushViewController(categorySelectViewController, animated: false)
        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
