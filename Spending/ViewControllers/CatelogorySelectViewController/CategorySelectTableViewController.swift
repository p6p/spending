//
//  CategorySelectTableViewController.swift
//  Spending
//
//  Created by master on 2018/1/2.
//  Copyright © 2018年 test. All rights reserved.
//

import UIKit
import RealmSwift
import QMUIKit

class CategorySelectTableViewController: UITableViewController {
    let realm = try! Realm()
    var incomeCategories : [RecordCategory] = []
    var outcomeCategories : [RecordCategory] = []
    var isIncome = true
    var editMode = false
    
    // 新設計使用 delegate 進行類別選擇通知
    var delegate: CategorySelectDelegate?
    
    init() {
        super.init(style: .grouped)
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // 將 categories 分成 income 和 outcome 兩類
        let categories = realm.objects(RecordCategory.self)
        for i in categories {
            if i.isIncome {
                incomeCategories.append(i)
            } else {
                outcomeCategories.append(i)
            }
        }
        
        // table view
        tableView.register(UITableViewCell.self, forCellReuseIdentifier: "reuseIdentifier")
        
        if editMode {
            navigationItem.rightBarButtonItem = UIBarButtonItem(barButtonSystemItem: .add, target: self, action: #selector(touchAddBtn))
        }
    }

    // MARK: - Table view data source

    override func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }

    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if isIncome {
            return incomeCategories.count
        } else {
            return outcomeCategories.count
        }
    }
    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let row = indexPath.row
        
        // 按照 segControl 選擇取出 income 或 outcome
        let category = isIncome ? incomeCategories[row] : outcomeCategories[row]
        
        let cell = tableView.dequeueReusableCell(withIdentifier: "reuseIdentifier", for: indexPath)
        cell.imageView?.image = UIImage(named: category.iconName)
        cell.textLabel?.text = category.name

        return cell
    }
    
    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let row = indexPath.row

        if delegate != nil { // 交由 delegate 進行處理
            let category = isIncome ? incomeCategories[row] : outcomeCategories[row]
            delegate?.selectedNewCategory(newCategory: category)
            dismiss(animated: true, completion: nil)
        } else { // deleteIt 相容之前版本的程式碼
            if !editMode {
                let category = isIncome ? incomeCategories[row] : outcomeCategories[row]
                
                // 傳遞回 AddRecordViewController
                let navVc = presentingViewController as! AddRecordNavigationController
                let vc = navVc.topViewController as! AddRecordViewController
                vc.category = category
                dismiss(animated: true, completion: nil)
            } else {
                tableView.deselectRow(at: indexPath, animated: true)
            }
        }
        
    }
    
    @objc func touchAddBtn() {
        navigationController?.pushViewController(NewCategoryViewController(), animated: true)
    }
    
    @objc func touchSegControl() {
        tableView.reloadData()
    }

}
