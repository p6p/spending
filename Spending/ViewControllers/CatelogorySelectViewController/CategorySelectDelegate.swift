//
//  CategorySelectDelegate.swift
//  Spending
//
//  Created by master on 2018/3/5.
//  Copyright © 2018年 test. All rights reserved.
//

import Foundation

protocol CategorySelectDelegate {
    func selectedNewCategory(newCategory: RecordCategory)
}

