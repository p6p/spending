//
//  CategorySelectViewController.swift
//  Spending
//
//  Created by master on 2018/3/6.
//  Copyright © 2018年 test. All rights reserved.
//

import UIKit

class CategorySelectViewController: UIViewController, UIToolbarDelegate {
    var delegate: CategorySelectDelegate?
    
    func position(for bar: UIBarPositioning) -> UIBarPosition {
        return .top
    }
    
    @objc func touchSegment(sender: UISegmentedControl) {
        switch  sender.selectedSegmentIndex {
        case 0:
            categorySelectTableViewController.isIncome = true
        case 1:
            categorySelectTableViewController.isIncome = false
        default:
            break
        }
        categorySelectTableViewController.touchSegControl()
    }

    let categorySelectTableViewController = CategorySelectTableViewController()

    override func viewDidLoad() {
        super.viewDidLoad()

        let toolBar = UIToolbar()
        
        toolBar.delegate = self
        toolBar.barTintColor = QDThemeManager.sharedInstance().currentTheme.themeTintColor()
        view.addSubview(toolBar)
        toolBar.snp.makeConstraints { (make) in
            make.left.right.equalTo(view)
            make.top.equalTo(topLayoutGuide.snp.bottom)
            make.height.equalTo(40)
        }
        let tabBarContainerView = UIView()
        tabBarContainerView.snp.makeConstraints { (make) in
            make.width.equalTo(UIScreen.main.bounds.width)
            make.height.equalTo(50)
        }
        
        let segmentControl = UISegmentedControl(items: ["收入".localized(), "支出".localized()])
        tabBarContainerView.addSubview(segmentControl)
        segmentControl.selectedSegmentIndex = 0
        segmentControl.tintColor = QDThemeManager.sharedInstance().currentTheme.segmentControlTintColor()
        segmentControl.addTarget(self, action: #selector(touchSegment), for: .valueChanged)
        segmentControl.snp.makeConstraints { (make) in
            make.left.equalTo(tabBarContainerView)
            make.right.equalTo(tabBarContainerView)
            make.centerX.equalTo(tabBarContainerView)
            make.top.equalTo(tabBarContainerView)
        }
        
        let barItem = UIBarButtonItem(customView: tabBarContainerView)
        toolBar.items = [barItem]
        
        
        // child View Controller
        addChildViewController(categorySelectTableViewController)
        view.addSubview(categorySelectTableViewController.view)
        categorySelectTableViewController.didMove(toParentViewController: self)
        categorySelectTableViewController.delegate = delegate
        categorySelectTableViewController.view.snp.makeConstraints { (make) in
            make.left.right.bottom.equalTo(view)
            make.top.equalTo(toolBar.snp.bottom)
        }
        
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
