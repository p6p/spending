//
//  MainTabBarController.swift
//  Spending
//
//  Created by master on 2018/1/14.
//  Copyright © 2018年 test. All rights reserved.
//

import UIKit
import Localize_Swift
import QMUIKit

class MainTabBarController: QMUITabBarViewController {

    override func viewDidLoad() {
        super.viewDidLoad()

        let homeViewController = HomeNavigationController()
        homeViewController.tabBarItem = UITabBarItem(title: "首頁".localized(), image: #imageLiteral(resourceName: "bonds"), selectedImage: #imageLiteral(resourceName: "bonds-filled"))
        homeViewController.tabBarItem.accessibilityIdentifier = "首頁"
        
        let listViewController = RecordListNavigationController()
        listViewController.tabBarItem = UITabBarItem(title: "明細".localized(), image: #imageLiteral(resourceName: "summary-list"), selectedImage: #imageLiteral(resourceName: "summary-list-filled"))
        listViewController.tabBarItem.accessibilityIdentifier = "流水"
        
        let chartViewController = ChartNavigationController()
        chartViewController.tabBarItem = UITabBarItem(title: "圖表".localized(), image: #imageLiteral(resourceName: "bar-chart"), selectedImage: #imageLiteral(resourceName: "bar-chart-filled"))
        chartViewController.tabBarItem.accessibilityIdentifier = "圖表"
        
        let settingViewController = SettingNavigationController()
        settingViewController.tabBarItem = UITabBarItem(title: "更多".localized(), image: #imageLiteral(resourceName: "more"), selectedImage: #imageLiteral(resourceName: "more-filled"))
        
        viewControllers = [homeViewController, listViewController, chartViewController, settingViewController]

        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
