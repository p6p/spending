//
//  PlusIntroductionViewController.swift
//  Spending
//
//  Created by master on 2018/1/19.
//  Copyright © 2018年 test. All rights reserved.
//

import UIKit
import GoogleMobileAds

class PlusIntroductionViewController: VipIntroductionViewController, GADRewardBasedVideoAdDelegate, UIPopoverPresentationControllerDelegate {
    var plusPoint = 0 {
        didSet {
            plusPointLabel.text = "Plus 點數：\(plusPoint)"
        }
    }
    
    let plusPointLabel = UILabel()
    let getPointBtn = UIButton()
    
    let path = NSHomeDirectory() + "/Documents/c04z04l3372e8ibeb9llf.rea1m"
    let kPointValue = "pointValue"
    
    #if DEBUG
    let kAdUnitID = "ca-app-pub-3940256099942544/1712485313"
    #else
    let kAdUnitID = "ca-app-pub-3953762937028581/2321644308"
    #endif
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        titleLabel.text = "Plus 特權 Beta"
        
        if UserManger.sharedInstance.getUserType() == .plus {
            let deadlineLabel = UILabel()
            view.addSubview(deadlineLabel)
            deadlineLabel.text = "到期日期：\(UserManger.sharedInstance.getExpireDate())"
            deadlineLabel.textColor = UIColor.white
            deadlineLabel.font = UIFont.systemFont(ofSize: 10)
            deadlineLabel.translatesAutoresizingMaskIntoConstraints = false
            deadlineLabel.centerXAnchor.constraint(equalTo: view.centerXAnchor).isActive = true
            deadlineLabel.topAnchor.constraint(equalTo: titleLabel.bottomAnchor, constant: 5).isActive = true
        }
        
        
        if FileManager.default.isReadableFile(atPath: path) {
            let uncheckedPoint = UserDefaults.standard.integer(forKey: kPointValue)
            let pStr = try! String(contentsOfFile: path, encoding: String.Encoding(rawValue: String.Encoding.utf8.rawValue))
            plusPoint = check(uncheckedPoint, pStr) ? uncheckedPoint : 0
        }
        
        view.addSubview(plusPointLabel)
        plusPointLabel.text = "我的點數：\(plusPoint)"
        plusPointLabel.textColor = UIColor.white
        plusPointLabel.translatesAutoresizingMaskIntoConstraints = false
        plusPointLabel.centerXAnchor.constraint(equalTo: view.centerXAnchor).isActive = true
        plusPointLabel.topAnchor.constraint(equalTo: titleLabel.bottomAnchor, constant: 50).isActive = true
        
        let helpBtn = UIButton()
        view.addSubview(helpBtn)
        helpBtn.setImage(#imageLiteral(resourceName: "help"), for: .normal)
        helpBtn.tintColor = UIColor.white
        helpBtn.addTarget(self, action: #selector(touchHelpBtn), for: .touchUpInside)
        helpBtn.translatesAutoresizingMaskIntoConstraints = false
        helpBtn.leadingAnchor.constraint(equalTo: plusPointLabel.trailingAnchor, constant: 10).isActive = true
        helpBtn.centerYAnchor.constraint(equalTo: plusPointLabel.centerYAnchor).isActive = true
        
        view.addSubview(getPointBtn)
        getPointBtn.isEnabled = false
        getPointBtn.setTitle("免費獲得獲得點數", for: .normal)
        getPointBtn.addTarget(self, action: #selector(playViedo), for: .touchUpInside)
        getPointBtn.translatesAutoresizingMaskIntoConstraints = false
        getPointBtn.topAnchor.constraint(equalTo: plusPointLabel.bottomAnchor, constant: 10).isActive = true
        getPointBtn.centerXAnchor.constraint(equalTo: view.centerXAnchor).isActive = true
        
        // preload Ad
        GADRewardBasedVideoAd.sharedInstance().delegate = self
        loadAd()
        
        //        let descriptionLabel = UILabel()
        //        view.addSubview(descriptionLabel)
        //        descriptionLabel.text = "50 點 plus 點數可兌換 7 天時長"
        //        descriptionLabel.textColor = UIColor.white
        //        descriptionLabel.translatesAutoresizingMaskIntoConstraints = false
        //        descriptionLabel.centerXAnchor.constraint(equalTo: view.centerXAnchor, constant: -30).isActive = true
        //        descriptionLabel.topAnchor.constraint(equalTo: getPointBtn.bottomAnchor, constant: 20).isActive = true
        //
        //        let exchangeBtn = UIButton()
        //        view.addSubview(exchangeBtn)
        //        exchangeBtn.setTitle("兌換", for: .normal)
        //        exchangeBtn.addTarget(self, action: #selector(touchExchangeBtn), for: .touchUpInside)
        //        exchangeBtn.translatesAutoresizingMaskIntoConstraints = false
        //        exchangeBtn.leadingAnchor.constraint(equalTo: descriptionLabel.trailingAnchor, constant: 5).isActive = true
        //        exchangeBtn.centerYAnchor.constraint(equalTo: descriptionLabel.centerYAnchor).isActive = true

    }
    
    // MARK: delegate
    func loadAd() {
        GADRewardBasedVideoAd.sharedInstance().load(GADRequest(), withAdUnitID: kAdUnitID)
    }
    
    func rewardBasedVideoAd(_ rewardBasedVideoAd: GADRewardBasedVideoAd,
                            didRewardUserWith reward: GADAdReward) {
        plusPoint += 1
        UserDefaults.standard.set(plusPoint, forKey: kPointValue)
        let text = encrypt(plusPoint)
        
        let result = FileManager.default.createFile(atPath: path, contents: nil, attributes: nil)
        let filePath = URL(fileURLWithPath: path)
        try! text.write(to: filePath, atomically: true, encoding: .utf8)
        loadAd()
        getPointBtn.isEnabled = false
    }
    
    func rewardBasedVideoAd(_ rewardBasedVideoAd: GADRewardBasedVideoAd, didFailToLoadWithError error: Error) {
        loadAd()
    }
    
    func rewardBasedVideoAdDidReceive(_ rewardBasedVideoAd:GADRewardBasedVideoAd) {
        getPointBtn.isEnabled = true
    }
    
    // pop
    func adaptivePresentationStyle(for controller: UIPresentationController) -> UIModalPresentationStyle {
        return .none
    }
    
    @objc func touchHelpBtn(sender: UIButton) {
        let pop = HelpViewController()
        pop.preferredContentSize = CGSize(width: 200, height: 150)
        pop.modalPresentationStyle = .popover
        pop.popoverPresentationController?.delegate = self
        pop.popoverPresentationController?.sourceView = sender
        pop.popoverPresentationController?.sourceRect = sender.bounds
        pop.popoverPresentationController?.permittedArrowDirections = .up
        self.present(pop, animated: true, completion: nil)
    }
    
    @objc func touchExchangeBtn() {
        if plusPoint >= 50 {
            plusPoint -= 50
        }
    }
    
    @objc func playViedo() {
        if GADRewardBasedVideoAd.sharedInstance().isReady {
            GADRewardBasedVideoAd.sharedInstance().present(fromRootViewController: self)
        }
    }
    

}
