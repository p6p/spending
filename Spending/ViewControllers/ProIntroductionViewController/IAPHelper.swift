//
//  IAPHelper.swift
//  Spending
//
//  Created by master on 2018/2/21.
//  Copyright © 2018年 test. All rights reserved.
//

import StoreKit

class ProductsRequest: NSObject, SKProductsRequestDelegate {
    var products : [SKProduct] = []
    
    func productsRequest(_ request: SKProductsRequest, didReceive response: SKProductsResponse) {
        products = response.products
        if response.invalidProductIdentifiers.count != 0 {
            print(response.invalidProductIdentifiers.description)
        }
    }
}

class ReceiptVerify: NSObject {
    
    func base64DataFromLocal() -> String {
        let userDefaults = UserDefaults.standard
        let dataURL = Bundle.main.appStoreReceiptURL // 收據在 iPhone 位置
        
        let base64Data = try! Data(contentsOf: dataURL!) // Base64 Data
        let base64Str = base64Data.base64EncodedString(options: .endLineWithLineFeed) // Base64 String
        userDefaults.set(base64Str, forKey: kBase64Str) // 儲存到 user defaults
        
        return base64Str

    }
    
    func base64DataFromUserDefaults() -> String {
        let userDefaults = UserDefaults.standard
        let base64Str = userDefaults.string(forKey: kBase64Str)
        
        return base64Str!
    }
    
    
    // 從 UserDefaults 讀取 base64, 向 Apple 驗證收據
    func verifyTransation(fromeUserDefault: Bool = false, isSnadbox: Bool = false, success: (()->Void)?, fail: (()->Void)?) {
        let base64Str = fromeUserDefault ? base64DataFromUserDefaults() : base64DataFromLocal()
        let bodyDict = ["receipt-data": base64Str, "password" : "bfe0872b2013429c88d2b68e68ecdf9b"] // 請求參數
        let bodyData = try! JSONSerialization.data(withJSONObject: bodyDict, options: .prettyPrinted)
        
        #if DEBUG
            let requestURL = URL(string: "https://sandbox.itunes.apple.com/verifyReceipt") // 沙盒 URL
        #else
            let requestURL = isSnadbox ? URL(string: "https://sandbox.itunes.apple.com/verifyReceipt") : URL(string: "https://buy.itunes.apple.com/verifyReceipt") // 生產環境 URL
        #endif
        var request = URLRequest(url: requestURL!)
        request.httpMethod = "POST"
        request.httpBody = bodyData
        URLSession.shared.dataTask(with: request) { (data, reponse, error) in
            let dict = try! JSONSerialization.jsonObject(with: data!, options: .mutableLeaves) as! Dictionary<String, Any>
            if dict["status"] as! Int == 0 {
                // unlock
                let latestReceiptInfo = dict["latest_receipt_info"]! as! Array<Dictionary<String, Any>>
                let lastReceiptInfo = latestReceiptInfo.last!
                let expiresDateMs = UInt64(lastReceiptInfo["expires_date_ms"]! as! String)!
                let expireDate = Date(timeIntervalSince1970: TimeInterval(expiresDateMs/1000))
                
                if expireDate > Date() {
                    UserManger.sharedInstance.set(expireDate: expireDate)
                    UserManger.sharedInstance.set(userType: .pro)
                    success?()
                } else {
                    UserManger.sharedInstance.set(userType: .pro)
                    UserManger.sharedInstance.set(expireDate: expireDate)
                    fail?()
                }
            } else if dict["status"] as! Int == 21007 {
                self.verifyTransation(isSnadbox: true, success: success, fail: fail)
            }
        }.resume()
    }
}
