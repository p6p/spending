//
//  PlusPointHelper.swift
//  Spending
//
//  Created by master on 2018/1/19.
//  Copyright © 2018年 test. All rights reserved.
//

import Foundation

let kExpireDate = "boxSize"
let kUserType = "BuildVersion"

func decodeExpireDate(_ timeInterval: UInt64) -> UInt64 {
    return (timeInterval - 71) / 53
}

func encodeExpireDate(_ timeInterval: UInt64) -> UInt64 {
    return timeInterval * 53 + 71
}


func sha1(_ string: String) -> String {
    let messageData = string.data(using:.utf8)!
    var digestData = Data(count: Int(CC_SHA1_DIGEST_LENGTH))
    
    _ = digestData.withUnsafeMutableBytes {digestBytes in
        messageData.withUnsafeBytes {messageBytes in
            CC_SHA1(messageBytes, CC_LONG(messageData.count), digestBytes)
        }
    }
    
    let output = NSMutableString(capacity: Int(CC_SHA1_DIGEST_LENGTH))
    for byte in digestData {
        output.appendFormat("%02x", byte)
    }
    
    return output as String
}

func encrypt(_ count: Int) -> String {
    return sha1(String(count) + "㊙️🆓")
}

func check(_ count: Int, _ string: String) -> Bool {
    return sha1(String(count) + "㊙️🆓") == string
}
