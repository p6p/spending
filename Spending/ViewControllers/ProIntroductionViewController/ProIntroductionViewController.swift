//
//  ProIntroductionViewController.swift
//  Spending
//
//  Created by master on 2018/1/19.
//  Copyright © 2018年 test. All rights reserved.
//

import UIKit
import StoreKit
import SafariServices
import QMUIKit

let kBase64Str = "yybbc"

// 購買請求流程
// 1. 進入 ViewController 時請求 product
// 2. 點擊購買，通過 product 生成訂單
// 3. 購買成功進行狀態更新

class ProIntroductionViewController: VipIntroductionViewController {
    // MARK: - ----------- ViewController Life ------------------
    let freeView = FreeView()
    let proView = UIView()
    override func viewDidLoad() {
        super.viewDidLoad()
        view.addSubview(freeView)
        freeView.snp.makeConstraints { (make) in
            make.top.equalTo(topLayoutGuide.snp.bottom)
            make.height.equalTo(200)
            make.leading.trailing.equalTo(view)
        }
        freeView.layoutSubviews()

        view.addSubview(proView)
        proView.snp.makeConstraints { (make) in
            make.top.equalTo(topLayoutGuide.snp.bottom)
            make.bottom.left.right.equalTo(view)
        }

        titleLabel.text = "Pro 特權".localized()

        
        // IAP Product 請求
        #if DEBUG
            showFreeUserUI()
        #else
            if UserManger.sharedInstance.getUserType() == .free {
                showFreeUserUI()
                
                if SKPaymentQueue.canMakePayments() { // 確定沒有開啟功能限制
                    QMUITips.showLoading(in: view)
                    IapMangaer.sharedInstance.requestProduct() {
                        QMUITips.hideAllTips(in: self.view)
                    }
                } else { // 提示用戶，開啟了取用限制
                    
                }
            } else {
                showProUserUI()
            }
        #endif
    }
    
    // MARK: - ------------UI----------------
    func showFreeUserUI() {
        freeView.isHidden = false
        proView.isHidden = true
        
        freeView.buyBtn.addTarget(self, action: #selector(buy), for: .touchUpInside)
        freeView.restoreBtn.addTarget(self, action: #selector(restore), for: .touchUpInside)
        freeView.privacyBtn.addTarget(self, action: #selector(touchPrivacyBtn), for: .touchUpInside)
        freeView.termBtn.addTarget(self, action: #selector(touchTermBtn), for: .touchUpInside)
    }
    
    func showProUserUI() {
        freeView.isHidden = true
        proView.isHidden = false
        
        let deadlineLabel = UILabel()
        proView.addSubview(deadlineLabel)
        deadlineLabel.text = "到期日期：\(UserManger.sharedInstance.getExpireDate())"
        //        deadlineLabel.textColor = UIColor.white
        deadlineLabel.font = UIFont.systemFont(ofSize: 10)
        deadlineLabel.translatesAutoresizingMaskIntoConstraints = false
        deadlineLabel.centerXAnchor.constraint(equalTo: view.centerXAnchor).isActive = true
        deadlineLabel.topAnchor.constraint(equalTo: titleLabel.bottomAnchor, constant: 5).isActive = true
        
        let thankLabel = UILabel()
        proView.addSubview(thankLabel)
        thankLabel.text = "感謝訂閱😀".localized()
        thankLabel.translatesAutoresizingMaskIntoConstraints = false
        thankLabel.centerXAnchor.constraint(equalTo: view.centerXAnchor).isActive = true
        thankLabel.topAnchor.constraint(equalTo: view.topAnchor, constant: 100).isActive = true
    }
    
    // MARK: - btn event
    @objc func buy() {
        QMUITips.showLoading(in: view)
        IapMangaer.sharedInstance.buy(success: {
            QMUITips.hideAllTips(in: self.view)
            DispatchQueue.main.sync() {
                self.showProUserUI()
            }
            let alertController = UIAlertController(title: "訂購成功", message: "感謝定購😀", preferredStyle: .alert)
            let okAction = UIAlertAction(title: "好的", style: .default, handler: nil)
            alertController.addAction(okAction)
            self.present(alertController, animated: true, completion: nil)
        }) {
            QMUITips.hideAllTips(in: self.view)
            let alertController = UIAlertController(title: "訂閱已過期", message: "請重新進行訂閱", preferredStyle: .alert)
            let okAction = UIAlertAction(title: "好的", style: .default, handler: nil)
            alertController.addAction(okAction)
            self.present(alertController, animated: true, completion: nil)
        }
    }
    
    @objc func restore() {
        IapMangaer.sharedInstance.restore(success: {
            DispatchQueue.main.sync() {
                self.showProUserUI()
            }
            let alertController = UIAlertController(title: "訂購成功", message: "感謝定購😀", preferredStyle: .alert)
            let okAction = UIAlertAction(title: "好的", style: .default, handler: nil)
            alertController.addAction(okAction)
            self.present(alertController, animated: true, completion: nil)
        }) {
            let alertController = UIAlertController(title: "訂閱已過期", message: "請重新進行訂閱", preferredStyle: .alert)
            let okAction = UIAlertAction(title: "好的", style: .default, handler: nil)
            alertController.addAction(okAction)
            self.present(alertController, animated: true, completion: nil)
        }
//        QMUITips.showLoading(in: view)
    }
    
    @objc func touchTermBtn() {
        let url = URL(string: "https://utree0.github.io/terms-and-conditions")
        let safariVC = SFSafariViewController(url: url!)
        self.show(safariVC, sender: nil)
    }
    
    @objc func touchPrivacyBtn() {
        let url = URL(string: "https://utree0.github.io/privacy-policy")
        let safariVC = SFSafariViewController(url: url!)
        self.show(safariVC, sender: nil)
    }
}
