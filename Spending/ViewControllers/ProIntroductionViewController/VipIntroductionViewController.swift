//
//  ProIntroductionViewController.swift
//  Spending
//
//  Created by master on 2018/1/7.
//  Copyright © 2018年 test. All rights reserved.
//

import UIKit
import SnapKit
import Localize_Swift
import QMUIKit

class VipIntroductionViewController: QMUICommonViewController {
    // MARK: - QMUINavigationControllerDelegate
//    override func shouldSetStatusBarStyleLight() -> Bool {
//        return false
//    }
//
//    override func titleViewTintColor() -> UIColor? {
//        return QMUIConfiguration.sharedInstance()?.blackColor
//    }
//
//    override func navigationBarTintColor() -> UIColor? {
//        return QMUIConfiguration.sharedInstance()?.whiteColor
//    }
//
//    override func navigationBarBackgroundImage() -> UIImage? {
//        return UIImage.qmui_image(with: UIColor.clear)
//    }
    
    let titleLabel = UILabel()

    override func viewDidLoad() {
        super.viewDidLoad()
        view.backgroundColor = UIColor.white
        
        let functionTitleLabel = UILabel()
        view.addSubview(functionTitleLabel)
        functionTitleLabel.text = "特權".localized()
        functionTitleLabel.translatesAutoresizingMaskIntoConstraints = false
        functionTitleLabel.snp.makeConstraints { (make) in
            make.top.equalTo(topLayoutGuide.snp.bottom).offset(220)
            make.centerX.equalTo(view)
        }
        
        let decorationView = UIView()
        view.addSubview(decorationView)
        decorationView.backgroundColor = mainColor1
        decorationView.translatesAutoresizingMaskIntoConstraints = false
        decorationView.snp.makeConstraints { (make) in
            make.width.equalTo(functionTitleLabel)
            make.height.equalTo(2)
            make.top.equalTo(functionTitleLabel.snp.bottom)
            make.centerX.equalTo(view)
        }
        
        // function stack view
        let fStackView = UIStackView()
        view.addSubview(fStackView)
        fStackView.axis = .vertical
        fStackView.translatesAutoresizingMaskIntoConstraints = false
        fStackView.snp.makeConstraints { (make) in
            make.centerX.equalTo(view)
            make.top.equalTo(decorationView.snp.bottom).offset(20)
            make.left.equalTo(view).offset(20)
            make.right.equalTo(view).offset(-20)
        }
        
        let iconList = [#imageLiteral(resourceName: "ad")]
        let textList = ["去廣告".localized()]
        let count = iconList.count
        
        for i in 0..<Int(ceil(Double(count) / 2.0)) {
            let rStackView = UIStackView()
            fStackView.addArrangedSubview(rStackView)
            rStackView.distribution = .fillEqually
            for j in 0..<2 {
                let contentView = UIView()
                rStackView.addArrangedSubview(contentView)
                contentView.translatesAutoresizingMaskIntoConstraints = false
                contentView.snp.makeConstraints({ (make) in
                    make.height.equalTo(80)
                })
                
                let index = i * 2 + j
                // icon
                if index < count {
                    let imageView = UIImageView(image: iconList[index])
                    contentView.addSubview(imageView)
                    imageView.translatesAutoresizingMaskIntoConstraints = false
                    imageView.snp.makeConstraints({ (make) in
                        make.centerX.top.equalTo(contentView)
                    })
                    
                    // label
                    let label = UILabel()
                    contentView.addSubview(label)
                    label.text = textList[index]
                    label.font = UIFont.systemFont(ofSize: 12)
                    label.translatesAutoresizingMaskIntoConstraints = false
                    label.snp.makeConstraints({ (make) in
                        make.centerX.equalTo(contentView)
                        make.top.equalTo(imageView.snp.bottom)
                    })
                }
            }
        }
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    // MARK: btn event
    @objc func close() {
        dismiss(animated: true, completion: nil)
    }

}
