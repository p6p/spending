//
//  swift
//  Spending
//
//  Created by master on 2018/2/21.
//  Copyright © 2018年 test. All rights reserved.
//

import UIKit
import SnapKit

class FreeView: UIView {
    let buyBtn = UIButton()
    let restoreBtn = UIButton()
    let privacyBtn = UIButton()
    let termBtn = UIButton()
    let desciprtionView = UILabel()


    override init(frame: CGRect) {
        super.init(frame: frame)

        addSubview(buyBtn)
        buyBtn.setTitle("150 元/月".localized(), for: .normal)
        buyBtn.setTitleColor(UIColor.white, for: .normal)
        buyBtn.titleLabel?.font = UIFont.systemFont(ofSize: 14)
        buyBtn.backgroundColor = mainColor1
        let buyLayer = buyBtn.layer
        buyLayer.cornerRadius = 3
        
        addSubview(restoreBtn)
        restoreBtn.setTitleColor(UIColor.gray, for: .normal)
        restoreBtn.setTitle("恢復購買".localized(), for: .normal)
        restoreBtn.titleLabel?.font = UIFont.systemFont(ofSize: 12)
        
        addSubview(privacyBtn)
        privacyBtn.setTitle("隱私權政策".localized(), for: .normal)
        privacyBtn.setTitleColor(UIColor.lightGray, for: .normal)
        privacyBtn.titleLabel?.font = UIFont.systemFont(ofSize: 12)
        
        addSubview(termBtn)
        termBtn.setTitle("使用條款".localized(), for: .normal)
        termBtn.setTitleColor(UIColor.lightGray, for: .normal)
        termBtn.titleLabel?.font = UIFont.systemFont(ofSize: 12)
        termBtn.translatesAutoresizingMaskIntoConstraints = false
        termBtn.snp.makeConstraints { (make) in
            make.centerX.equalTo(self).offset(50)
            make.centerY.equalTo(privacyBtn)
        }
        
        addSubview(desciprtionView)
        desciprtionView.text = "itunesDesciption".localized()
        desciprtionView.font = UIFont.systemFont(ofSize: 10)
        desciprtionView.textColor = UIColor.lightGray
        desciprtionView.lineBreakMode = .byWordWrapping
        desciprtionView.numberOfLines = 0
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    override func layoutSubviews() {
        super.layoutSubviews()
        buyBtn.translatesAutoresizingMaskIntoConstraints = false
        buyBtn.snp.makeConstraints { (make) in
            make.centerX.equalTo(self)
            make.top.equalTo(self).offset(20)
            make.width.equalTo(120)
            make.height.equalTo(50)
        }
        
        restoreBtn.translatesAutoresizingMaskIntoConstraints = false
        restoreBtn.snp.makeConstraints { (make) in
            make.centerX.equalTo(self)
            make.top.equalTo(buyBtn.snp.bottom).offset(2)
        }
        
        privacyBtn.translatesAutoresizingMaskIntoConstraints = false
        privacyBtn.snp.makeConstraints { (make) in
            make.centerX.equalTo(self).offset(-50)
            make.top.equalTo(restoreBtn.snp.bottom).offset(10)
        }
        
        
        desciprtionView.translatesAutoresizingMaskIntoConstraints = false
        desciprtionView.snp.makeConstraints { (make) in
            make.top.equalTo(privacyBtn.snp.bottom).offset(5)
            make.width.equalTo(300)
            make.centerX.equalTo(self)
        }
    }
    
    /*
    // Only override draw() if you perform custom drawing.
    // An empty implementation adversely affects performance during animation.
    override func draw(_ rect: CGRect) {
        // Drawing code
    }
    */

}
