//
//  ProPageViewController.swift
//  Spending
//
//  Created by master on 2018/1/14.
//  Copyright © 2018年 test. All rights reserved.
//

import UIKit
import SnapKit

class ProPageViewController: UIPageViewController, UIPageViewControllerDelegate, UIPageViewControllerDataSource {
    let pageControl = UIPageControl()
    let pViewControllers : [UIViewController] = [ProIntroductionViewController()]
    
    func pageViewController(_ pageViewController: UIPageViewController, didFinishAnimating finished: Bool, previousViewControllers: [UIViewController], transitionCompleted completed: Bool) {
        pageControl.currentPage = pViewControllers.index(of: viewControllers!.first!)!
    }
    
    
    func pageViewController(_ pageViewController: UIPageViewController, viewControllerBefore viewController: UIViewController) -> UIViewController? {
        if viewController.view.tag > 0 {
            pageControl.currentPage = viewController.view.tag-1
            return pViewControllers[viewController.view.tag-1]
        }
        return nil
    }
    
    func pageViewController(_ pageViewController: UIPageViewController, viewControllerAfter viewController: UIViewController) -> UIViewController? {
        if viewController.view.tag < pViewControllers.count - 1 {
            pageControl.currentPage = viewController.view.tag+1
            return pViewControllers[viewController.view.tag+1]
        }
        return nil
    }
    
    init() {
        super.init(transitionStyle: .scroll, navigationOrientation: .horizontal, options: nil)
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        title = "開通 VIP".localized()
//        pViewControllers[1].isPlus = false
        delegate = self
        dataSource = self
        for i in 0..<pViewControllers.count {
            pViewControllers[i].view.tag = i
        }
        
        setViewControllers([pViewControllers[0]], direction: .forward, animated: true, completion: nil)

        pageControl.pageIndicatorTintColor = UIColor.lightGray
        pageControl.currentPageIndicatorTintColor = UIColor.black
        pageControl.numberOfPages = pViewControllers.count
        view.addSubview(pageControl)
        pageControl.translatesAutoresizingMaskIntoConstraints = false
        pageControl.centerXAnchor.constraint(equalTo: view.centerXAnchor).isActive = true
        pageControl.bottomAnchor.constraint(equalTo: view.bottomAnchor, constant: -20).isActive = true
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
