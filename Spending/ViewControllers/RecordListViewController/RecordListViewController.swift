//
//  ListViewController.swift
//  Spending
//
//  Created by master on 2017/12/23.
//  Copyright © 2017年 test. All rights reserved.
//

import UIKit
import RealmSwift
import Localize_Swift
import QMUIKit

class RecordListViewController: QMUICommonViewController {
    var tabBarMode = false
    // 目前顯示月份 viewDidLoad touchLastMonthBtn touchNextMonthBtn updateMonthLabel
    var recordsDate = Date()
    let calendar = Calendar.current
    
    // viewDidLoad updateMonthLabel
    let monthLabel = UILabel()

    let recordTableViewController = RecordListTableViewController()
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        recordTableViewController.tableView.reloadData()
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // background color
        view.backgroundColor = UIColor.white
        titleView.isUserInteractionEnabled = true
        titleView.snp.makeConstraints { (make) in
            make.size.equalTo(CGSize(width: 200, height: 40))
        }
        
        // 顯示目前月份
        titleView.addSubview(monthLabel)
        monthLabel.font = UIFont.boldSystemFont(ofSize: 16)
        monthLabel.textColor = UIColor.white
        monthLabel.translatesAutoresizingMaskIntoConstraints = false
        monthLabel.centerYAnchor.constraint(equalTo: titleView.centerYAnchor).isActive = true
        monthLabel.centerXAnchor.constraint(equalTo: titleView.centerXAnchor).isActive = true
        updateMonthLabel()
        
        // 上個月
        let lastMonthBtn = UIButton()
        titleView.addSubview(lastMonthBtn)
        lastMonthBtn.setImage(#imageLiteral(resourceName: "sort-left"), for: .normal)
        lastMonthBtn.tintColor = UIColor.white
        lastMonthBtn.translatesAutoresizingMaskIntoConstraints = false
        lastMonthBtn.centerYAnchor.constraint(equalTo: monthLabel.centerYAnchor).isActive = true
        lastMonthBtn.trailingAnchor.constraint(equalTo: monthLabel.leadingAnchor, constant: -5).isActive = true
        lastMonthBtn.addTarget(self, action: #selector(touchLastMonthBtn), for: .touchUpInside)

        // 下個月
        let nextMonthBtn = UIButton()
        titleView.addSubview(nextMonthBtn)
        nextMonthBtn.setImage(#imageLiteral(resourceName: "sort-right"), for: .normal)
        nextMonthBtn.tintColor = UIColor.white
        nextMonthBtn.addTarget(self, action: #selector(touchNextMonthBtn), for: .touchUpInside)
        nextMonthBtn.translatesAutoresizingMaskIntoConstraints = false
        nextMonthBtn.centerYAnchor.constraint(equalTo: monthLabel.centerYAnchor).isActive = true
        nextMonthBtn.leadingAnchor.constraint(equalTo: monthLabel.trailingAnchor, constant: 5).isActive = true
        
        // 收支紀錄列表
        addChildViewController(recordTableViewController)
        view.addSubview(recordTableViewController.view)
        recordTableViewController.didMove(toParentViewController: self)
        recordTableViewController.view.snp.makeConstraints { (make) in
            make.top.equalTo(topLayoutGuide.snp.bottom)
            make.left.right.equalTo(view)
            make.bottom.equalTo(bottomLayoutGuide.snp.top).offset(-49)
        }
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
    // btn event
    
    @objc func touchLastMonthBtn() {
        recordTableViewController.lastMonth()
        recordsDate = calendar.date(byAdding: .month, value: -1, to: recordsDate)!
        updateMonthLabel()
    }
    
    @objc func touchNextMonthBtn() {
        recordTableViewController.nextMonth()
        recordsDate = calendar.date(byAdding: .month, value: 1, to: recordsDate)!
        updateMonthLabel()
    }
    
    // help function
    func updateMonthLabel() {
        let dateComponents = calendar.dateComponents([.year,.month, .day, .hour,.minute,.second], from: recordsDate )
        let currentLanguage = Localize.currentLanguage()
        if currentLanguage == "zh-Hans" || currentLanguage == "zh-Hant" {
            monthLabel.text = "\(dateComponents.year!) 年 \(dateComponents.month!) 月"
        } else {
            monthLabel.text = "\(dateComponents.year!) / \(dateComponents.month!)"
        }
        
    }
}
