//
//  RecordTableViewController.swift
//  Spending
//
//  Created by master on 2017/12/23.
//  Copyright © 2017年 test. All rights reserved.
//

import UIKit
import QMUIKit
import RealmSwift
import Localize_Swift

class RecordListTableViewController: QMUICommonTableViewController {
    var recordFrame : [[Record]] = []
    let calendar = Calendar.current
    var queryDate = Date()
    let realm = try! Realm()
    var mounthIncomeSum = 0.0
    var mounthOutcomeSum = 0.0
    
    var incomeSums: [Double] = [] // 該段時間，每天的支出
    var outcomeSums: [Double] = []

    init() {
        super.init(style: .plain)
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    var isNeedUpdate = false
    @objc func recordChanged() {
        isNeedUpdate = true
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        if isNeedUpdate {
            tableView.reloadData()
        }
    }
    
    var headerView: RecordListTableViewHeaderView?
    override func viewDidLoad() {
        super.viewDidLoad()
        NotificationCenter.default.addObserver(self, selector: #selector(recordChanged), name: UTRecordChanged, object: nil)

        // table view
        tableView.register(RecordListCell.self, forCellReuseIdentifier: "reuseIdentifier")
        tableView.separatorColor = ConfigurationManger.sharedInstance.recordListTableViewSeparatorColor
        
        headerView = RecordListTableViewHeaderView(frame: CGRect(x: 0, y: 0, width: tableView.frame.width, height: 57))
        tableView.tableHeaderView = headerView
    }

    // MARK: - ------------Table view data source------------------
    // section
    override func numberOfSections(in tableView: UITableView) -> Int {
        recordFrame = RangeRecordHelper.sharedInstance.getDayRecrordsList(queryDate: queryDate)
        (incomeSums, outcomeSums) = sumGroupRecord(recordFrame)
        mounthIncomeSum = incomeSums.reduce(0, +)
        mounthOutcomeSum = outcomeSums.reduce(0, +)
        headerView?.set(mounthIncome: mounthIncomeSum, mounthOutcome: mounthOutcomeSum)
        
        if recordFrame.count == 0 {
            showEmptyView(withText: "該月沒有紀錄".localized(), detailText: nil, buttonTitle: nil, buttonAction: nil)
        } else {
            hideEmptyView()
        }
        return recordFrame.count
    }
    
    // section title
    override func tableView(_ tableView: UITableView, titleForHeaderInSection section: Int) -> String? {
        let dateFormmatter = DateFormatter()
        dateFormmatter.setLocalizedDateFormatFromTemplate("M 月 d 日 (eeee)")
        let date = recordFrame[section][0].date
        let result = dateFormmatter.string(from: date)
        return result
    }
    
    func generateRecordListSumLabel() -> QMUILabel {
        let currentTheme = QDThemeManager.sharedInstance().currentTheme!
        let label = QMUILabel()
        label.frame.size = CGSize(width: 200, height: 20)
        label.textAlignment = .right
        let textSize = currentTheme.recordListTableViewSumLabelTextSize()
        label.font = UIFont.systemFont(ofSize: textSize)
        label.textColor = currentTheme.recordListTableViewSumLabelTextColor()
        
        return label
    }
    
    // section height
    override func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        return ConfigurationManger.sharedInstance.recordListTableViewSectionHeight
    }
    
    // section view
    override func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        let headerView = super.tableView(tableView, viewForHeaderInSection: section) as! QMUITableViewHeaderFooterView
//        headerView.backgroundView?.backgroundColor = UIColor.clear
        let sumLabel = generateRecordListSumLabel()
        headerView.accessoryView = sumLabel

        sumLabel.text = "支出".localized() + ":\(incomeSums[section]) " + "收入".localized() + ":\(outcomeSums[section])"
        
        return headerView
    }
    
    // row height
    override func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return RecordListCell.cellHeight
    }

    // row number
    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return recordFrame[section].count
    }

    // cell
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "reuseIdentifier", for: indexPath) as! RecordListCell
        
        let indexNum = indexPath.row
        let section = indexPath.section
        
        let categoryName = (recordFrame[section][indexNum].category?.name)!
        let categoryImageName = (recordFrame[section][indexNum].category?.iconName)!
        let isIncome = (recordFrame[section][indexNum].category?.isIncome)!
        let amount = recordFrame[section][indexNum].sum
        
        cell.set(categoryName: categoryName, categoryImageName: categoryImageName, isIncome: isIncome, amount: amount)

        return cell
    }
    
    // can editable
    override func tableView(_ tableView: UITableView, canEditRowAt indexPath: IndexPath) -> Bool {
        return true
    }
    
    override func tableView(_ tableView: UITableView, commit editingStyle: UITableViewCellEditingStyle, forRowAt indexPath: IndexPath) {
        if editingStyle == .delete {
            let section = indexPath.section
            let row = indexPath.row
            let record : Record? = recordFrame[section][row]
            recordFrame[section].remove(at: row)
            
            tableView.beginUpdates()
            if recordFrame[section].count == 0 {
                recordFrame.remove(at: section)
                tableView.deleteSections([section], with: .automatic)
            }
            tableView.deleteRows(at: [indexPath], with: .automatic)
            tableView.endUpdates()
            try! realm.write {
                realm.delete(record!)
            }
        }
    }
    
    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        tableView.deselectRow(at: indexPath, animated: true)
        let section = indexPath.section
        let row = indexPath.row
        let record : Record = recordFrame[section][row]
        
        // update
        present(AddRecordNavigationController(record: record), animated: true, completion: nil)
    }
    
    func sumGroupRecord(_ groupRecord: [[Record]]) -> ([Double], [Double]) {
        var incomeSums: [Double] = [] // 該段時間，每天的支出
        var outcomeSums: [Double] = []
        
        for recordsList in groupRecord {
            // 某一天的所有紀錄
            var incomeSum = 0.0 // 當天收入總和
            var outcomeSum = 0.0 // 當天支出總和
            for record in recordsList {
                if record.category!.isIncome {
                    incomeSum += record.sum
                } else {
                    outcomeSum += record.sum
                }
            }
            incomeSums.append(incomeSum)
            outcomeSums.append(outcomeSum)
        }
        
        return (incomeSums, outcomeSums)
    }
    
    func nextMonth() {
        queryDate = calendar.date(byAdding: .month, value: 1, to: queryDate)!
        tableView.reloadData()
    }
    
    func lastMonth() {
        queryDate = calendar.date(byAdding: .month, value: -1, to: queryDate)!
        tableView.reloadData()
    }
}
