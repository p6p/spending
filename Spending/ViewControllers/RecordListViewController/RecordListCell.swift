//
//  ListCell.swift
//  Spending
//
//  Created by master on 2017/12/31.
//  Copyright © 2017年 test. All rights reserved.
//

import UIKit

class RecordListCell: UITableViewCell {
    private let sumLabel = UILabel()
    private let categoryNameLabel = UILabel()
    private let iconView = UIImageView()
    
    func set(categoryName: String, categoryImageName: String, isIncome: Bool, amount: Double) {
        let rawImage = UIImage(named: categoryImageName)
        iconView.image = rawImage?.qmui_imageResized(inLimitedSize: CGSize(width: 30, height: 30))
        
        categoryNameLabel.text = categoryName
 
        if isIncome {
            sumLabel.textColor = sumRed
        } else {
            sumLabel.textColor = sumGreen
        }
        
        sumLabel.text = "$ \(doubleToString(amount))"
    }

    override init(style: UITableViewCellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)
        // 收入支出類別圖標
        addSubview(iconView)
        iconView.snp.makeConstraints { (make) in
            make.centerY.equalTo(self)
            make.left.equalTo(self).offset(ConfigurationManger.sharedInstance.paddingHorizontal)
        }
        
        // 收入支出類別
        addSubview(categoryNameLabel)
        categoryNameLabel.font = UIFont.systemFont(ofSize: ConfigurationManger.sharedInstance.recordListCellCategoryNameLabelTextSize)
        categoryNameLabel.snp.makeConstraints { (make) in
            make.centerY.equalTo(self)
            make.left.equalTo(iconView.snp.right).offset(ConfigurationManger.sharedInstance.spacingHorizontal)
        }

        // 收入支出金額
        addSubview(sumLabel)
        sumLabel.font = UIFont.systemFont(ofSize: ConfigurationManger.sharedInstance.recordListCellSumLabelTextSize)
        sumLabel.snp.makeConstraints { (make) in
            make.centerY.equalTo(self)
            make.right.equalTo(self).offset(-ConfigurationManger.sharedInstance.paddingHorizontal)
        }
    }
    
    static let cellHeight: CGFloat = ConfigurationManger.sharedInstance.recordListCellHeight
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
}
