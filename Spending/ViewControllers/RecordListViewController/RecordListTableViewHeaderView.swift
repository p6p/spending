//
//  RecordListTableViewHeaderView.swift
//  Spending
//
//  Created by master on 2018/3/4.
//  Copyright © 2018年 test. All rights reserved.
//

import UIKit
import QMUIKit

class RecordListTableViewHeaderView: UIView {
    let incomeLabel = UILabel()
    let outcomeLabel = UILabel()
    
    func generateTitleLabel() -> QMUILabel {
        let label = QMUILabel()
        label.font = UIFont.systemFont(ofSize: 12)
        label.textColor = UIColor.gray
        
        return label
    }
    
    func set(mounthIncome: Double, mounthOutcome: Double) {
        incomeLabel.text = "$ \(mounthIncome)"
        outcomeLabel.text = "$ \(mounthOutcome)"
    }

    override init(frame: CGRect) {
        super.init(frame: frame)
        let incomeTitleLabel = generateTitleLabel()
        addSubview(incomeTitleLabel)
        incomeTitleLabel.text = "收入".localized()
        incomeTitleLabel.snp.makeConstraints { (make) in
            make.top.equalTo(self).offset(10)
            make.left.equalTo(self).offset(ConfigurationManger.sharedInstance.paddingHorizontal)
        }
        
        addSubview(incomeLabel)
        incomeLabel.text = "0"
        incomeLabel.font = UIFont.systemFont(ofSize: 16)
        incomeLabel.textColor = UIColor.darkGray
        incomeLabel.snp.makeConstraints { (make) in
            make.left.equalTo(incomeTitleLabel)
            make.top.equalTo(incomeTitleLabel.snp.bottom).offset(5)
        }
        
        let outcomeTitleLabel = generateTitleLabel()
        addSubview(outcomeTitleLabel)
        outcomeTitleLabel.text = "支出".localized()
        outcomeTitleLabel.snp.makeConstraints { (make) in
            make.top.equalTo(incomeTitleLabel)
            make.left.equalTo(incomeTitleLabel.snp.right).offset(100)
        }
        
        addSubview(outcomeLabel)
        outcomeLabel.text = "0"
        outcomeLabel.font = UIFont.systemFont(ofSize: 16)
        outcomeLabel.textColor = UIColor.darkGray
        outcomeLabel.snp.makeConstraints { (make) in
            make.left.equalTo(outcomeTitleLabel)
            make.top.equalTo(incomeLabel)
        }
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
}
