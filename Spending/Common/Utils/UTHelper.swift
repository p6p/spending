//
//  UTHelper.swift
//  Spending
//
//  Created by master on 2018/2/18.
//  Copyright © 2018年 test. All rights reserved.
//

import UIKit
import QMUIKit

class UTHelper {
    func generateLightBorderedButton() -> QMUIButton {
        let btn = QMUIButton()
        btn.frame.size = CGSize(width: 200, height: 40)
        btn.titleLabel?.font = UIFont.boldSystemFont(ofSize: 14)
//        btn.setTitleColor(<#T##color: UIColor?##UIColor?#>, for: .normal)
//        btn.backgroundColor = UIColor
//        btn.highlightedBackgroundColor = UIColor
        btn.layer.borderWidth = 1
        btn.layer.cornerRadius = 4
//        btn.highlightedBorderColor = UIColor
        
        return btn
    }
}
