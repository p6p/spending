//
//  QDThemeProtocol.h
//  qmuidemo
//
//  Created by MoLice on 2017/5/9.
//  Copyright © 2017年 QMUI Team. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <QMUIKit/QMUIKit.h>

/// 所有主题均应实现这个协议，规定了 QMUI Demo 里常用的几个关键外观属性
@protocol QDThemeProtocol <QMUIConfigurationTemplateProtocol>

@required

- (UIColor *)themeTintColor;
- (UIColor *)themeListTextColor;
- (UIColor *)themeCodeColor;
- (UIColor *)themeGridItemTintColor;

- (UIColor *)homeBackgroundColor;
- (UIColor *)homeButtonColor;
- (UIColor *)addIncomeRecordBackgroundColor;
- (UIColor *)addOutcomeRecordBackgroundColor;
- (UIColor *)addRecordTextColor;

- (NSString *)themeName;

- (UIColor *)segmentControlTintColor;

- (UIColor *)recordListTableViewSumLabelTextColor;
- (CGFloat)recordListTableViewSumLabelTextSize;

@end


/// 所有能响应主题变化的对象均应实现这个协议，目前主要用于 QDCommonViewController 及 QDCommonTableViewController
@protocol QDChangingThemeDelegate <NSObject>

@required

- (void)themeBeforeChanged:(NSObject<QDThemeProtocol> *)themeBeforeChanged afterChanged:(NSObject<QDThemeProtocol> *)themeAfterChanged;

@end
