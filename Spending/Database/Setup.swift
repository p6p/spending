//
//  Setup.swift
//  Spending
//
//  Created by master on 2017/12/26.
//  Copyright © 2017年 test. All rights reserved.
//

import Foundation
import RealmSwift

let kUseAuthentication = "useAuthentication"


func cleanAll() {
    let realm = try! Realm()
    try! realm.write {
        realm.deleteAll()
    }
}

// 清空並載入範例
func setupDemoRecord() {
    let realm = try! Realm()
    
    var records : [Record] = []
    
//    let salary = realm.objects(RecordCategory.self).first!
//    let food = realm.objects(RecordCategory.self).filter("iconNmae = %@", "food").first!
//    let shopping = realm.objects(RecordCategory.self).filter("iconNmae = %@", "shopping-cart").first!
//    let transport = realm.objects(RecordCategory.self).filter("iconNmae = %@", "car").first!
//    let billing = realm.objects(RecordCategory.self).filter("iconNmae = %@", "bill").first!
    let salary = realm.objects(RecordCategory.self).first!
    let food = realm.objects(RecordCategory.self)[2]
    let shopping = realm.objects(RecordCategory.self)[1]
    let transport = realm.objects(RecordCategory.self)[3]
    let billing = realm.objects(RecordCategory.self)[5]

    let today = Date()
    let yesterday = Calendar.current.date(byAdding: .day, value: -1, to: today)!
    
    records.append(Record(value: ["category" : food, "sum" : 158, "date" : yesterday]))
    records.append(Record(value: ["category" : food, "sum" : 235, "date" : yesterday]))
    records.append(Record(value: ["category" : transport, "sum" : 45, "date" : yesterday]))
    records.append(Record(value: ["category" : salary, "sum" : 50000, "date" : yesterday]))
    records.append(Record(value: ["category" : food, "sum" : 198, "date" : yesterday]))
    records.append(Record(value: ["category" : food, "sum" : 167, "date" : today]))
    records.append(Record(value: ["category" : shopping, "sum" : 2400, "date" : today]))
    records.append(Record(value: ["category" : shopping, "sum" : 3150, "date" : today]))
    records.append(Record(value: ["category" : food, "sum" : 270, "date" : today]))
    records.append(Record(value: ["category" : billing, "sum" : 500, "date" : today]))

    try! realm.write {
        for record in records {
            realm.add(record)
        }
    }
}

// 隨機產生包含兩位小數的一個數字，可指定開始和結束範圍
func randAmount(startNum: Double, endNum: Double) -> Double {
    let rawRand = arc4random()
    let range = UInt32((endNum - startNum)*100)
    let a = Double(arc4random() % range) + startNum * 100.0
    return a / 100.0
}

// 從某一天開始每天產生 10-20 筆隨機數據，直到結束日期
// 類別隨機產生，金額 10-20000 可包含小數兩位
func randomDemo() {
    let realm = try! Realm()

    let allCategory = realm.objects(RecordCategory.self)
    // 隨機產生一個類別，可能是收入或支出
    func randCategory() -> RecordCategory {
        let categoryCount = allCategory.count
        let randIndex = Int(arc4random()) % categoryCount
        
        return allCategory[randIndex]
    }
    
    func randRecordNum(startNum: UInt32, endNum: UInt32) -> UInt32 {
        return arc4random() % (endNum - startNum) + startNum
    }
    
    var records : [Record] = []
    
    // 時間都是一樣的
    var startDateComponent = DateComponents()
    startDateComponent.year = 2018
    startDateComponent.month = 1
    startDateComponent.day = 1
    
    let startDate = Calendar.current.date(from: startDateComponent)
    
    let durationRange = 365
    
    for i in 0..<durationRange {
        let currentDate = Calendar.current.date(byAdding: .day, value: i, to: startDate!)
        
        let recordNum = randRecordNum(startNum: 10, endNum: 20)
        for j in 0..<recordNum {
            print("generate \(currentDate)-\(j)")
            let category = randCategory()
            let sum = randAmount(startNum: 10, endNum: 20000)
            records.append(Record(value: ["category" : category, "sum" : sum, "date" : currentDate!]))
        }
        try! realm.write {
            for record in records {
                realm.add(record)
            }
        }
    }
    



//    let startDate
}

func setupUserDefault() {
    // 快速啟動 index
    let userDefaults = UserDefaults.standard
    let fastRecord = [1, 2, 3, 4, 5, 6, 7, 8]
    userDefaults.set(fastRecord, forKey: "fastRecord")
    userDefaults.set(false, forKey: kUseAuthentication)
    userDefaults.set("TWD", forKey: kCurrency)
    userDefaults.set(false, forKey: kIsReminderOn)
    userDefaults.synchronize()
}

func setupDefaultCategory() {
    let incomeCatlogories = [NSLocalizedString("薪資", comment: "薪資")]
    let incomeIcons = ["cash-in-hand"]
    let outcomeCatlogories = [NSLocalizedString("日常購物", comment: "日常購物"), NSLocalizedString("食物", comment: "食物"), NSLocalizedString("交通", comment: "交通"), NSLocalizedString("娛樂", comment: "娛樂"), NSLocalizedString("帳單", comment: "帳單"), NSLocalizedString("醫療", comment: "醫療"), NSLocalizedString("聚會", comment: "聚會"), NSLocalizedString("存款", comment: "存款")]
    let outcomeIcons = ["shopping-cart", "food", "car", "music", "bill", "recovery", "party-baloon", "money-box"]
    
    let realm = try! Realm()

    for i in 0...incomeCatlogories.count-1 {
        let incomeCatlogory = RecordCategory()
        incomeCatlogory.name = incomeCatlogories[i]
        incomeCatlogory.isIncome = true
        incomeCatlogory.iconName = incomeIcons[i]
        try! realm.write {
            realm.add(incomeCatlogory)
        }
    }
    
    for i in 0...outcomeCatlogories.count-1 {
        let outcomeCatlogory = RecordCategory()
        outcomeCatlogory.name = outcomeCatlogories[i]
        outcomeCatlogory.isIncome = false
        outcomeCatlogory.iconName = outcomeIcons[i]
        try! realm.write {
            realm.add(outcomeCatlogory)
        }
    }
}

