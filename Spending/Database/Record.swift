//
//  Record.swift
//  Spending
//
//  Created by master on 2017/12/31.
//  Copyright © 2017年 test. All rights reserved.
//

import Foundation
import RealmSwift

class Record : Object {
    @objc dynamic var sum : Double = 0
    @objc dynamic var date = Date()
    @objc dynamic var category : RecordCategory?
    @objc dynamic var detail = ""
}
