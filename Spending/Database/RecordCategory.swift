//
//  Category.swift
//  Spending
//
//  Created by master on 2017/12/31.
//  Copyright © 2017年 test. All rights reserved.
//

import Foundation
import RealmSwift

class RecordCategory : Object {
    @objc dynamic var name = ""
    @objc dynamic var iconName = ""
    @objc dynamic var isIncome = false
}
