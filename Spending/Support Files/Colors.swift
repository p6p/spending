//
//  Colors.swift
//  Spending
//
//  Created by master on 2018/1/16.
//  Copyright © 2018年 test. All rights reserved.
//

import UIKit

func hexColor(_ hex: Int) -> UIColor {
    let red = (hex >> 16) & 0xFF
    let green =  (hex >> 8) & 0xFF
    let blue =  hex & 0xFF
    return UIColor(red: CGFloat(red)/255, green: CGFloat(green)/255, blue: CGFloat(blue)/255, alpha: 1.0)
}

let btnColor = hexColor(0x8D8F90)
let keyboardGray = hexColor(0x616161)
let mainColor = hexColor(0xEBEEF3)
let mainColor1 = hexColor(0xFD753C)
let sumGreen = hexColor(0x8CAF6E)
let sumRed = hexColor(0x9F5359)
let incomeColor = hexColor(0xEF5350)
let outcomeColor = hexColor(0x66BB6A)
