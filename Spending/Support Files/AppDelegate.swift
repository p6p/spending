//
//  AppDelegate.swift
//  Spending
//
//  Created by master on 2017/12/23.
//  Copyright © 2017年 test. All rights reserved.
//

import UIKit
import RealmSwift
import Firebase
import LocalAuthentication
import FirebaseDatabase
import UserNotifications
import TOPasscodeViewController
import GoogleMobileAds

@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate, UNUserNotificationCenterDelegate, TOPasscodeViewControllerDelegate {
    let passWindow = UIWindow(frame: UIScreen.main.bounds)

    var window: UIWindow?
    let userDefaults = UserDefaults.standard
    
    func userNotificationCenter(_ center: UNUserNotificationCenter, willPresent notification: UNNotification, withCompletionHandler completionHandler: @escaping (UNNotificationPresentationOptions) -> Void) {
        completionHandler([.badge, .sound, .alert])
    }

    func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplicationLaunchOptionsKey: Any]?) -> Bool {
        
        // migration
        let realmConfig = Realm.Configuration(
            // 设置新的架构版本。必须大于之前所使用的
            // （如果之前从未设置过架构版本，那么当前的架构版本为 0）
            schemaVersion: 1,
            
            // 设置模块，如果 Realm 的架构版本低于上面所定义的版本，
            // 那么这段代码就会自动调用
            migrationBlock: { migration, oldSchemaVersion in
                // 我们目前还未执行过迁移，因此 oldSchemaVersion == 0
                if (oldSchemaVersion < 1) {
                    // 没有什么要做的！
                    // Realm 会自行检测新增和被移除的属性
                    // 然后会自动更新磁盘上的架构
                    migration.enumerateObjects(ofType: Record.className()) { oldObject, newObject in
                        // 将两个 name 合并到 fullName 当中
                        let intSum = oldObject!["sum"] as! Int
                        newObject!["sum"] = Double(intSum)
                    }
                }
        })
        
        // 通知 Realm 为默认的 Realm 数据库使用这个新的配置对象
        Realm.Configuration.defaultConfiguration = realmConfig
        
        // realm complete check
        do {
            let _ = try Realm()
        } catch {
            print(error)
            Realm.Configuration.defaultConfiguration.deleteRealmIfMigrationNeeded = true
            setupDefaultCategory()
        }
        
        // first time setup
        
        if !userDefaults.bool(forKey: "hasBeenLaunched") {
            print("first launch setup")
            userDefaults.set(true, forKey: "hasBeenLaunched")
            userDefaults.synchronize()
            
            setupDefaultCategory()
            setupUserDefault()
            
            // 生產一整年的數據
            #if DEBUG
                randomDemo()
            #endif
        }
        
        // user Init
        IapMangaer.sharedInstance.recheckSubscription(success: nil, fail: nil)
        
        // print realm path
        #if DEBUG
            let docPath = NSSearchPathForDirectoriesInDomains(.documentDirectory, .userDomainMask, true)[0]
            print(docPath)
            print()
        #endif
        
        #if DEBUG
//            cleanAll()
//            setupDefaultCategory()
//            setupDemoRecord()
        #endif
        
        // window
        self.window = UIWindow(frame: UIScreen.main.bounds)
        self.window?.backgroundColor = UIColor.white

        // 指定 root view controller
        self.window?.rootViewController = MainTabBarController()
        self.window?.makeKeyAndVisible()
        
        // Use Firebase library to configure APIs
        FirebaseApp.configure()
        
        // Initialize the Google Mobile Ads SDK.
        GADMobileAds.configure(withApplicationID: "ca-app-pub-3953762937028581~3107211009")
        
        touchId()
        
        // Cloud control
//        var ref: DatabaseReference!
//
//        ref = Database.database().reference()
//        ref.child("message").observeSingleEvent(of: .value) { (snapshot) in
//            print(snapshot.value as! String)
//        }
        
        // Notification
        UNUserNotificationCenter.current().delegate = self
        UNUserNotificationCenter.current().requestAuthorization(options: [.alert, .sound, .badge], completionHandler: { granted, error in
        })
        
        // remote config
        let config = ConfigurationManger.sharedInstance
        config.setDefault()
        config.fetch()
        
//        產生隨機資料
//        randomDemo()
        
        // 關閉 satisfy constraints 警告
        #if DEBUG
            UserDefaults.standard.setValue(false, forKey:"_UIConstraintBasedLayoutLogUnsatisfiable")
        #endif

        return true
    }

    func applicationWillEnterForeground(_ application: UIApplication) {
        touchId()
    }
    
    func touchId() {
        if userDefaults.bool(forKey: kUseAuthentication) {
            let passwordController = TOPasscodeViewController(style: .translucentLight, passcodeType: .fourDigits)
            passwordController.delegate = self
            passwordController.rightAccessoryButton = UIButton()
            passWindow.makeKeyAndVisible()

            passWindow.windowLevel = 100
            passWindow.rootViewController = passwordController
            window?.windowLevel = 1
            passWindow.isHidden = false
        }
    }
    
    func passcodeViewController(_ passcodeViewController: TOPasscodeViewController, isCorrectCode code: String) -> Bool {
        let settingModel = SettingModel()
        if code == settingModel.launchPassword() {
            passWindow.rootViewController = nil
            passWindow.isHidden = true
            window?.makeKeyAndVisible()
            return true
        }
        return false
    }
}
