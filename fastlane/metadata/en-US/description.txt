Spending makes billing easier, making it easy with just learning in two simple steps. Through the chart function to grasp the daily and the United States and the moon's consumption, spend the situation at hand, how much money spent clearly show.

Special feature
Quick Accounting: Only two steps from the main screen to complete a billing
Chart: Grasp each spending cycle and the type of consumption
Passcode lock: protect the privacy of the account

Spending Premium

- $4.99/monthly

This subscription auto-renews at the end of each month term, less cancelled 24-hours in advance. The subscription fee is charged to your iTunes account at confirmation of purchase. You may manage your subscription and turn off auto-renewal by going to your settings. No cancellation of the current subscription is allowed during active period.

Privacy Policy: 
https://utree0.github.io/privacy-policy
Terms of Use: 
https://utree0.github.io/terms-and-conditions
