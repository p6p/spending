Spending 要让记帐变得更简单，只要两步就完成一笔记帐，超简单不用学就可以轻松上手。通过图表功能掌握每天和美月的消费状况，花费状况一手掌握，哪天花了多少钱清楚呈现。

特色功能
快速记帐：从主画面只要两步就可完成一笔记帐
统计图表：掌握每个周期的花费状况以及消费的种类
密码锁：保护帐本的隐私

Spending Premium

- $4.99/monthly

This subscription auto-renews at the end of each month term, less cancelled 24-hours in advance. The subscription fee is charged to your iTunes account at confirmation of purchase. You may manage your subscription and turn off auto-renewal by going to your settings. No cancellation of the current subscription is allowed during active period.

Privacy Policy: 
https://utree0.github.io/privacy-policy
Terms of Use: 
https://utree0.github.io/terms-and-conditions
